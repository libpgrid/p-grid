package TucGrid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;

import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class GridPeer {
    private static final Logger logger_ = LoggerFactory.getLogger(GridPeer.class);


    // arg[0] : host name
    // arg[1] : is bootstrap [T/F]
    // arg[2] : bootstrap peer ip
    // arg[3] : bootstrap peer port
    public static void main(String[] args) {
        logger_.debug("Hostname: {}", args[0]);

        String hostname = args[0];
        boolean isBootstrap = Boolean.parseBoolean(args[1]);
        PGridP2P p2p = null;
        try {
            NetworkInterface inet = NetworkInterface.getByName(hostname);
            if (inet != null) {
                for (InetAddress address : Collections.list(inet.getInetAddresses())) {
                    hostname = address.getHostName();
                }
            }
        } catch (SocketException e) {
            logger_.error("Network interface with name {} not found", hostname);

        }

        try {
            InetAddress byName = InetAddress.getByName(hostname);
            logger_.debug("Canonical hostname: {}", byName.getCanonicalHostName());
            logger_.debug("Hostname: {}", byName.getHostName());
            logger_.debug("Host address: {}", byName.getHostAddress());
            p2p = new PGridP2P(byName, 3000, isBootstrap);
        } catch (UnknownHostException e) {
            logger_.error("Given host name not found {}.", e);
            System.exit(3);
        }

        try {
            p2p.initializeP2PServices();
        } catch (Exception ex) {
            logger_.error("{}", ex);
            p2p.shutdown();
            System.exit(4);
        }
        
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new ShutdownThread(p2p));

        if (!isBootstrap) { // is this host not the bootstrap host?
            String bootstrapIP = args[2];
            int bootstrapPort = Integer.valueOf(args[3]);
            logger_.info("Bootstrapping with remote host {}:{}", bootstrapIP, bootstrapPort);
            p2p.join(bootstrapIP, bootstrapPort);
        } else {
            logger_.info("This is the bootstrapper peer");
        }
    }
}

/**
 * Because the simulation in the grid are stopped with a SIGTERM followed by a
 * SIGKILL, this serves as a shutdown handle for the JVM. It shutdowns the p-grid
 * p2p layer
 */
class ShutdownThread extends Thread {
    private static final Logger logger_ = LoggerFactory.getLogger(ShutdownThread.class);
    private final PGridP2P p2p_;

    public ShutdownThread(PGridP2P p2p) {
        p2p_ = p2p;
    }

    @Override
    public void run() {
        logger_.info("Shutting down using SIGTERM hook");
        try {
            p2p_.getLocalRoutingTable().store(p2p_.getLocalHost().getAddress().getHostName() + ".xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        p2p_.shutdown();
        logger_.info("Local peer info at shutdown: {}->{}",
                p2p_.getLocalHost().getHostPath(), p2p_.getLocalHost().getAddress().getHostName());
    }
}

