#!/bin/sh
#
#PBS -q tuc
#PBS -k oe
#PBS -N PGridBootstrapper
#PBS -l nodes=wn001.grid.tuc.gr

hostname="wn001.grid.tuc.gr"
isBootstrap="true"

java -jar /storage/tuclocal/nvourlakis/dist/libpgrid.jar $hostname $isBootstrap
