#/bin/sh
#
#PBS -q tuc
#PBS -k oe
#PBS -N PGridPeer
#PBS -l nodes=wn002.grid.tuc.gr

hostname="wn002.grid.tuc.gr"
isBootstrap="false"
server_ip="147.27.48.101"
port="3000"


java -jar /storage/tuclocal/nvourlakis/dist/libpgrid.jar $hostname $isBootstrap $server_ip $port
