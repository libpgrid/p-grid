package pgrid.utilities;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * This is taken from Google Guava library. It's a temporary steal till I put
 * the Guava dependency through maven.
 *
 * @author Erik Kline
 */
public class InetAddressUtils {
    /**
     * Returns the IPv4 address embedded in an IPv4 compatible address.
     *
     * @param ip {@link java.net.Inet6Address} to be examined for an embedded
     *           IPv4 address
     * @return {@link java.net.Inet4Address} of the embedded IPv4 address
     * @throws IllegalArgumentException if the argument is not a valid
     *                                  IPv4 compatible address
     */
    public static Inet4Address getCompatIPv4Address(Inet6Address ip) {
        return getInet4Address(Arrays.copyOfRange(ip.getAddress(), 12, 16));
    }

    /**
     * Returns an {@link Inet4Address}, given a byte array representation
     * of the IPv4 address.
     *
     * @param bytes byte array representing an IPv4 address (should be
     *              of length 4).
     * @return {@link Inet4Address} corresponding to the supplied byte
     *         array.
     * @throws IllegalArgumentException if a valid {@link Inet4Address}
     *                                  can not be created.
     */
    private static Inet4Address getInet4Address(byte[] bytes) {
        InetAddress ipv4 = null;
        try {
            ipv4 = InetAddress.getByAddress(bytes);
            if (!(ipv4 instanceof Inet4Address)) {
                throw new UnknownHostException(
                        String.format("'%s' is not an IPv4 address.",
                                ipv4.getHostAddress()));
            }
        } catch (UnknownHostException e) {
            /*
            * This really shouldn't happen in practice since all our byte
            * sequences should be valid IP addresses.
            *
            * However {@link InetAddress#getByAddress} is documented as
            * potentially throwing this "if IP address is of illegal length".
            *
            * This is mapped to IllegalArgumentException since, presumably,
            * the argument triggered some bizarre processing bug.
            */
            throw new IllegalArgumentException(
                    String.format("Host address '%s' is not a valid IPv4 address.",
                            Arrays.toString(bytes)),
                    e);

        }
        return (Inet4Address) ipv4;
    }
}
