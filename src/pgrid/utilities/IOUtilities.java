package pgrid.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class IOUtilities {
    private static final Logger logger_ = LoggerFactory.getLogger(IOUtilities.class);

    public static void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException ex) {
            logger_.error("{}", ex);
        }
    }

}
