package pgrid.utilities;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class is used to configure the log4j framework. Things changed when
 * slf4j was discovered. This class is deprecated and will be removed.
 */
@Deprecated
public class Log4jConfigurator {

    private static boolean configured = false;

    public synchronized static void configure() {
        if (!configured) {
            //BasicConfigurator.configure();
            //PropertyConfigurator.configure("log4j.conf");
            DOMConfigurator.configure("logback.xml");
            configured = true;
        }
    }

    public synchronized static void shutdown() {
        if (configured) {
            LogManager.shutdown();
            configured = false;
        }
    }
}
