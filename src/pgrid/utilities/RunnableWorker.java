package pgrid.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class that adds a cancel mechanism to a Runnable. The class that
 * inherits only need to implement {@link RunnableWorker#workload()} method.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public abstract class RunnableWorker implements Runnable {

    private static final Logger logger_ = LoggerFactory.getLogger(RunnableWorker.class);

    private volatile Thread blinker_ = null;

    @Override
    public final void run() {
        Thread thisThread = Thread.currentThread();
        blinker_ = thisThread;
        while (thisThread == blinker_) {
            workload();
        }
        logger_.debug("RunnableWorker is exiting.");
    }

    /**
     * Cancels this Runnable that runs on a Thread.
     */
    public final void cancel() {
        blinker_ = null;
    }

    /**
     * This method must implement the actual work that the
     * {@link Runnable} will call in its {@link Runnable#run()} method.
     */
    public abstract void workload();
}
