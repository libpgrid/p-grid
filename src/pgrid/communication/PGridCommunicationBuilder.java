package pgrid.communication;

import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.CORBAImpl.ORBFactory;

/**
 * This class is responsible to instantiate the communication layer of the
 * P-Grid peer.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridCommunicationBuilder {
    private static final Logger logger_ = LoggerFactory.getLogger(PGridCommunicationBuilder.class);

    private final PGridProxy proxy_;
    private final PGridEndpoint endpoint_;

    /**
     * Given a {@link PGridP2P} interface, the address and the port that the
     * local peer will run on, it constructs a builder object. When the object
     * is constructed, the client can ask for the {@link PGridProxy} and the
     * {@link PGridEndpoint} instances.
     *
     * @param p2p     the PGridP2P interface.
     * @param address the ip that the local peer will run on.
     * @param port    the port that the local peer will run.
     */
    public PGridCommunicationBuilder(PGridP2P p2p, String address, int port) {
        logger_.info("Initializing CORBA ORB on {}:{}", address, port);
        ORB orb_ = ORBFactory.getORB(address, port);
        proxy_ = new PGridProxyImpl(p2p, orb_);
        endpoint_ = new PGridEndpointImpl(p2p, orb_);
    }

    /**
     * After the construction of this builder, it returns the constructed
     * {@link PGridProxy} instance.
     *
     * @return the proxy.
     */
    public PGridProxy getProxyInstance() {
        return proxy_;
    }

    /**
     * After the construction of this builder, it returns the constructed
     * {@link PGridEndpoint} instance.
     *
     * @return the endpoint.
     */
    public PGridEndpoint getEndpointInstance() {
        return endpoint_;
    }
}
