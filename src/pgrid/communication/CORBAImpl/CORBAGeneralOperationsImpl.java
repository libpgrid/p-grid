package pgrid.communication.CORBAImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.CORBAServices.CORBAGeneralOperationsPOA;
import pgrid.communication.CORBAServices.PeerReference;
import pgrid.communication.CORBAUtilities.Deserializer;
import pgrid.communication.CORBAUtilities.Serializer;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class CORBAGeneralOperationsImpl extends CORBAGeneralOperationsPOA {

    private static final Logger logger_ = LoggerFactory.getLogger(CORBAGeneralOperationsImpl.class);

    private final PGridP2P p2p_;

    public CORBAGeneralOperationsImpl(PGridP2P p2p) {
        p2p_ = p2p;
    }

    @Override
    public PeerReference peer() {
        return Serializer.serializePGridHost(p2p_.getLocalHost());
    }

    @Override
    public boolean isOnline(PeerReference peer) {
        logger_.debug("Asked if I'm online from {}:{}.", peer.address, peer.port);
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(peer)); // recv request msg
        p2p_.timedLocalEvent(); // send response msg

        return true;
    }

    @Override
    public PeerReference getPeer(PeerReference peer) {
        logger_.debug("Asked about local information from {}:{}.", peer.address, peer.port);
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(peer)); // recv request msg
        p2p_.timedLocalEvent(); // send response msg

        return Serializer.serializePGridHost(p2p_.getLocalHost());
    }

    @Override
    public PeerReference[] getRoutingTable(PeerReference peer) {
        logger_.debug("Asked about local routing table from {}:{}.", peer.address, peer.port);
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(peer));
        p2p_.timedLocalEvent(); // send response msg

        return Serializer.serializeRoutingTable(p2p_.getLocalRoutingTable());
    }
}
