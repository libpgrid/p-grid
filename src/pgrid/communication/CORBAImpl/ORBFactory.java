package pgrid.communication.CORBAImpl;

import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ORBFactory {
    private static final Logger logger_ = LoggerFactory.getLogger(ORBFactory.class);
    private static ORB orb_ = null;

    public static synchronized ORB getORB(String initialHost, int initialPort) {
        if (orb_ == null) {
            Properties props = new Properties();
            props.put("com.sun.CORBA.POA.ORBPersistentServerPort",
                    Integer.toString(initialPort));
            props.put("com.sun.CORBA.POA.ORBPersistentServerHost",
                    initialHost);
            logger_.info("Constructing ORB on {}:{}", initialHost, initialPort);
            orb_ = ORB.init((String[]) null, props);
        }
        return orb_;
    }

    public static synchronized ORB getORB() {
        if (orb_ == null) {
            logger_.info("Constructing ORB with default values");
            orb_ = ORB.init((String[]) null, null);
        }
        return orb_;
    }
}
