package pgrid.communication.CORBAImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.CORBAServices.CORBAMaintenancePOA;
import pgrid.communication.CORBAServices.ExchangeInfo;
import pgrid.communication.CORBAServices.PeerReference;
import pgrid.communication.CORBAUtilities.Deserializer;
import pgrid.communication.CORBAUtilities.Serializer;
import pgrid.core.PGridPeer;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class CORBAMaintenanceImpl extends CORBAMaintenancePOA {

    private static final Logger logger_ = LoggerFactory.getLogger(CORBAMaintenanceImpl.class);

    private final PGridP2P p2p_;

    public CORBAMaintenanceImpl(PGridP2P p2p) {
        p2p_ = p2p;
    }

    @Override
    public PeerReference peer() {
        return Serializer.serializePGridHost(p2p_.getLocalHost());
    }

    @Override
    public String maintenanceState(PeerReference peer) {
        logger_.info("Corba layer received maintenance state request from {}:{}", peer.address, peer.port);

        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(peer)); // recv request msg
        p2p_.timedLocalEvent(); // send response msg

        return p2p_.getMaintainer().maintenanceState();
    }

    /**
     * ************ EXCHANGE ******************************************
     */

    @Override
    public ExchangeInfo exchangeInfo(ExchangeInfo info) {
        logger_.info("Corba layer received exchange info from {}:{}", info.peer.address, info.peer.port);

        PGridPeer remotePeer = Deserializer.deserializeExchangeInfo(info);

        p2p_.timedRemoteEvent(remotePeer.getHost()); // recv request msg
        p2p_.timedLocalEvent(); // send response msg

        p2p_.getMaintainer().exchangeRequest(remotePeer);

        return new ExchangeInfo(
                Serializer.serializePGridHost(p2p_.getLocalHost()),
                Serializer.serializeRoutingTable(p2p_.getLocalRoutingTable()));
    }

    /**
     * ************ REPAIR ********************************************
     */

    @Override
    public void fixNode(PeerReference peer, PeerReference failed, String path) {
        // TODO: Implement fixNode
        throw new UnsupportedOperationException();
    }

    @Override
    public PeerReference getFailedPeer(PeerReference peer) {
        // TODO: Implement getFailedPeer
        throw new UnsupportedOperationException();
    }

    @Override
    public void waitingSolution(PeerReference peer) {
        // TODO: Implement waitingSolution
        throw new UnsupportedOperationException();
    }

    @Override
    public void solutionNotify(PeerReference peer, PeerReference replacer, PeerReference conjugate) {
        // TODO: Implement solutionNotify
        throw new UnsupportedOperationException();
    }
}
