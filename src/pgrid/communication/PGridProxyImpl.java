package pgrid.communication;

import org.omg.CORBA.ORB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.CORBAServices.*;
import pgrid.communication.CORBAUtilities.Deserializer;
import pgrid.communication.CORBAUtilities.Serializer;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;

import java.net.InetAddress;

/**
 * Here I'll gather all the classes that have to do with sending a request from
 * the local peer to a remote peer.
 * Needs a factory.
 * <p/>
 * TODO: Process org.omg.CORBA.COMM_FAILURE exception (go repair the remote node that is!)
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridProxyImpl implements PGridProxy {
    // TODO: must be created from factory
    private static final Logger logger_ = LoggerFactory.getLogger(PGridProxyImpl.class);

    private final ORB orb_;
    private final PGridP2P p2p_;
    private final PGridHost localhost_;

    public PGridProxyImpl(PGridP2P p2p, ORB orb) {
        p2p_ = p2p;
        orb_ = orb;
        localhost_ = p2p_.getLocalHost();
    }

    @Override
    public boolean isOnline(String address, int port) {
        if (address == null) {
            throw new NullPointerException("Address cannot be null.");
        }
        if (port < 0) {
            throw new IllegalArgumentException("Port must be positive.");
        }
        
        logger_.info("Asking CORBA layer if {}:{} is online", address, port);
        CORBAGeneralOperations operations = getGeneralOperationsRef(address, port);

        p2p_.timedLocalEvent();
        boolean result = operations.isOnline(Serializer.serializePGridHost(localhost_));
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(operations.peer()));

        logger_.debug("Result received.");
        return result;
    }

    @Override
    public boolean isOnline(InetAddress address, int port) {
        if (address == null) {
            throw new NullPointerException("Address cannot be null.");
        }
        if (port < 0) {
            throw new IllegalArgumentException("Port must be positive.");
        }

        return isOnline(address.getHostAddress(), port);
    }

    @Override
    public PGridHost getRemoteHost(String address, int port) {
        if (address == null) {
            throw new NullPointerException("Address cannot be null.");
        }
        if (port < 0) {
            throw new IllegalArgumentException("Port must be positive.");
        }
        
        logger_.info("Asking CORBA layer who is on {}:{}", address, port);
        CORBAGeneralOperations operations = getGeneralOperationsRef(address, port);

        p2p_.timedLocalEvent();
        PeerReference serializedHost = operations.getPeer(Serializer.serializePGridHost(localhost_));
        PGridHost remoteHost = Deserializer.deserializePGridHost(serializedHost);
        p2p_.timedRemoteEvent(remoteHost);

        logger_.debug("PeerReference received.");
        return remoteHost;
    }

    @Override
    public RoutingTable getRemoteRT(String address, int port) {
        if (address == null) {
            throw new NullPointerException("Address cannot be null.");
        }
        if (port < 0) {
            throw new IllegalArgumentException("Port must be positive.");
        }

        logger_.info("Asking CORBA layer for the routing table of {}:{}", address, port);
        CORBAGeneralOperations operations = getGeneralOperationsRef(address, port);

        p2p_.timedLocalEvent();
        PeerReference[] serializedRT = operations.getRoutingTable(Serializer.serializePGridHost(localhost_));
        logger_.debug("PeerReference array received.");
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(operations.peer()));

        RoutingTable rt = Deserializer.deserializeRoutingTable(serializedRT);

        logger_.debug("Routing table constructed successfully.");
        return rt;
    }

    @Override
    public String maintenanceState(PGridHost remoteHost) {
        if (remoteHost == null) {
            throw new NullPointerException();
        }

        logger_.info("Asking CORBA layer for maintenance state of {}", remoteHost);
        CORBAMaintenance maintenance =
                getMaintenanceRef(remoteHost.getAddress().getHostName(), remoteHost.getPort());

        p2p_.timedLocalEvent();
        String state = maintenance.maintenanceState(Serializer.serializePGridHost(localhost_));
        p2p_.timedRemoteEvent(Deserializer.deserializePGridHost(maintenance.peer()));

        logger_.debug("Received maintenance state: {}", state);
        return state;
    }

    @Override
    public PGridPeer exchangeInfo(PGridHost remoteHost, PGridHost localHost, RoutingTable localRT) {
        if (remoteHost == null || localHost == null || localRT == null) {
            throw new NullPointerException();
        }
        
        logger_.info("Asking CORBA layer for exchange information of {}", remoteHost);
        CORBAMaintenance maintenance =
                getMaintenanceRef(remoteHost.getAddress().getHostAddress(), remoteHost.getPort());

        p2p_.timedLocalEvent();
        ExchangeInfo localInfo = new ExchangeInfo(
                Serializer.serializePGridHost(localHost),
                Serializer.serializeRoutingTable(localRT));
        ExchangeInfo remoteInfo = maintenance.exchangeInfo(localInfo);
        PGridPeer remotePeer = Deserializer.deserializeExchangeInfo(remoteInfo);
        p2p_.timedRemoteEvent(remotePeer.getHost());

        logger_.debug("Received exchange information for {}", remotePeer.getHost());
        return remotePeer;
    }

    @Override
    public PGridHost getFailedPeer(PGridHost remoteHost) {
        return null;
    }

    @Override
    public void waitingSolution(PGridHost remoteHost, PGridHost localHost) {

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private CORBAGeneralOperations getGeneralOperationsRef(String address, int port) {
        String[] generalOpsID = CORBAGeneralOperationsHelper.id().split(":");
        String corbaloc = "corbaloc:iiop:[" +
                address + "]:" + port + "/" + generalOpsID[1];
        //"/pgrid/communication/CORBAServices/CORBAGeneralOperations";
        logger_.debug("CORBALOC: {}", corbaloc);

        org.omg.CORBA.Object object = orb_.string_to_object(corbaloc);
        return CORBAGeneralOperationsHelper.narrow(object);
    }

    private CORBAMaintenance getMaintenanceRef(String address, int port) {
        String[] maintenanceID = CORBAMaintenanceHelper.id().split(":");
        String corbaloc = "corbaloc:iiop:[" +
                address + "]:" + port + "/" + maintenanceID[1];
        //"/pgrid/communication/CORBAServices/CORBAMaintenance";
        logger_.debug("CORBALOC: {}", corbaloc);

        org.omg.CORBA.Object object = orb_.string_to_object(corbaloc);
        return CORBAMaintenanceHelper.narrow(object);
    }
}
