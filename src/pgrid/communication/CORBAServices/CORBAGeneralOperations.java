package pgrid.communication.CORBAServices;


/**
 * pgrid/communication/CORBAServices/CORBAGeneralOperations.java .
 * Generated by the IDL-to-Java compiler (portable), version "3.2"
 * from pgrid_core.idl
 * Σάββατο, 8 Οκτώβριος 2011 6:56:04 μμ EEST
 */

public interface CORBAGeneralOperations extends CORBAGeneralOperationsOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity {
} // interface CORBAGeneralOperations
