package pgrid.communication.CORBAServices;


/**
 * pgrid/communication/CORBAServices/CORBAGeneralOperationsOperations.java .
 * Generated by the IDL-to-Java compiler (portable), version "3.2"
 * from pgrid_core.idl
 * Σάββατο, 8 Οκτώβριος 2011 6:56:04 μμ EEST
 */

public interface CORBAGeneralOperationsOperations {

    /**
     * The owner of this remote object.
     */
    pgrid.communication.CORBAServices.PeerReference peer();

    /**
     * Asking a remote peer to see if he is alive.
     *
     * @param peer the local peer used for updates by the remote..
     * @return true if the remote peer is online or else false.
     */
    boolean isOnline(pgrid.communication.CORBAServices.PeerReference peer);

    /**
     * Asking a remote peer to send information about him.
     *
     * @param peer the local peer used for updates by the remote.
     * @return the remote peer.
     */
    pgrid.communication.CORBAServices.PeerReference getPeer(pgrid.communication.CORBAServices.PeerReference peer);

    /**
     * Asking a remote peer to send information about his routing
     * table.
     *
     * @param peer the local peer used for updates by the remote.
     * @return the routing table of the remote peer.
     */
    pgrid.communication.CORBAServices.PeerReference[] getRoutingTable(pgrid.communication.CORBAServices.PeerReference peer);
} // interface CORBAGeneralOperationsOperations
