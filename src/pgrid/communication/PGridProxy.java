package pgrid.communication;

import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;

import java.net.InetAddress;

/**
 * All methods must work for both IPv4 and IPv6. To keep things simple when the
 * host name is known it is preferable to pass it instead of the ip address and
 * let the communication layer do the resolution.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface PGridProxy {
//    @Deprecated
//    public boolean exchangeInvitation(PGridHost host);
//
//    @Deprecated
//    public RoutingTable exchangeInitiation(PGridHost host);

    //******************** General operations ******************************
    public boolean isOnline(String address, int port);

    public boolean isOnline(InetAddress address, int port);

    public PGridHost getRemoteHost(String address, int port);

    public RoutingTable getRemoteRT(String address, int port);

    //******************** Maintenance *************************************
    public String maintenanceState(PGridHost remoteHost);

    public PGridPeer exchangeInfo(PGridHost remoteHost, PGridHost localHost, RoutingTable localRT);

    public PGridHost getFailedPeer(PGridHost remoteHost);

    public void waitingSolution(PGridHost remoteHost, PGridHost localHost);
}
