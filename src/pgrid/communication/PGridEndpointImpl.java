package pgrid.communication;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantAlreadyActive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.CORBAImpl.CORBAGeneralOperationsImpl;
import pgrid.communication.CORBAImpl.CORBAMaintenanceImpl;
import pgrid.communication.CORBAServices.CORBAGeneralOperationsHelper;
import pgrid.communication.CORBAServices.CORBAMaintenanceHelper;

/**
 * Implementation class of {@link PGridEndpoint}. This is class is responsible
 * to initialize the CORBA facility. It registers all the servants to the POA
 * and then opens the ORB to accept remote requests..
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridEndpointImpl implements PGridEndpoint {
    private static final Logger logger_ = LoggerFactory.getLogger(PGridEndpointImpl.class);
    private final PGridP2P pgridP2P_;
    private final ORB orb_;

    public PGridEndpointImpl(PGridP2P p2p, ORB orb) {
        pgridP2P_ = p2p;
        orb_ = orb;
    }

    @Override
    public void initializeServices() {
        try {
            POA rootPOA = null;
            try {
                rootPOA = POAHelper.narrow(orb_.resolve_initial_references("RootPOA"));
            } catch (InvalidName invalidName) {
                logger_.error("Invalid name while resolving RootPOA {}", invalidName);
            }

            rootPOA.the_POAManager().activate();

            // Initialize and register P-Grid general operations servant.
            CORBAGeneralOperationsImpl generalOpsServant =
                    new CORBAGeneralOperationsImpl(pgridP2P_);
            rootPOA.activate_object(generalOpsServant);
            String[] generalOpsID = CORBAGeneralOperationsHelper.id().split(":");
            ((com.sun.corba.se.impl.orb.ORBImpl) orb_).register_initial_reference(
                    generalOpsID[1],
                    rootPOA.servant_to_reference(generalOpsServant)
            );
            logger_.info("Initialized and registered: {}", generalOpsID[1]);

            // Initialize and register P-Grid maintenance services servant.
            CORBAMaintenanceImpl maintenanceServant =
                    new CORBAMaintenanceImpl(pgridP2P_);
            rootPOA.activate_object(maintenanceServant);
            String[] maintenanceID = CORBAMaintenanceHelper.id().split(":");
            ((com.sun.corba.se.impl.orb.ORBImpl) orb_).register_initial_reference(
                    maintenanceID[1],
                    rootPOA.servant_to_reference(maintenanceServant)
            );
            logger_.info("Initialized and registered: {}", maintenanceID[1]);

            logger_.info("CORBA services initialized successfully.");
        } catch (ServantNotActive servantNotActive) {
            logger_.error("Servant is not active {}", servantNotActive);
        } catch (WrongPolicy wrongPolicy) {
            logger_.error("Wrong policy {}", wrongPolicy);
        } catch (InvalidName invalidName) {
            logger_.error("Invalid name {}", invalidName);
        } catch (ServantAlreadyActive servantAlreadyActive) {
            logger_.error("Servant is already active {}", servantAlreadyActive);
        } catch (AdapterInactive adapterInactive) {
            logger_.error("Called operation while the POAManager is inactive {}", adapterInactive);
        }
    }

    @Override
    public void cancel() {
        logger_.info("ORB cancellation requested.");
        orb_.shutdown(false);
    }

    @Override
    public void run() {
        logger_.info("ORB is ready to start running.");
        orb_.run();
        logger_.info("ORB has stopped running.");
    }
}
