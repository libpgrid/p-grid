package pgrid.communication;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface PGridEndpoint extends Runnable {
    public void initializeServices();

    public void cancel();
}
