package pgrid.communication.CORBAUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.communication.CORBAServices.ExchangeInfo;
import pgrid.communication.CORBAServices.PeerReference;
import pgrid.communication.PGridProxyImpl;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;

import java.net.UnknownHostException;
import java.util.UUID;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Deserializer {

    private static final Logger logger_ = LoggerFactory.getLogger(PGridProxyImpl.class);

    /**
     * Transforms a {@link PeerReference} object from the CORBA layer to a
     * {@link PGridHost} object.
     *
     * @param ref the object to be deserialized.
     * @return the {@link PGridHost} object.
     */
    public static PGridHost deserializePGridHost(PeerReference ref) {
        logger_.debug("Transforming CORBA PeerReference to PGridHost");
        PGridHost host = null;
        try {
            host = new PGridHost(ref.address, ref.port);
            host.setHostPath(ref.path);
            host.setTimestamp(ref.timestamp);
            host.setUUID(UUID.fromString(ref.uuid));
        } catch (UnknownHostException e) {
            logger_.error("Tried to construct a PGridHost object with a malformed ip");
            // XXX: what else????
        }
        return host;
    }

    /**
     * Accepts and array of {@link PeerReference} and transform it to a
     * {@link RoutingTable} object.
     *
     * @param refs the array to be deserialized.
     * @return the {@link RoutingTable} object.
     */
    public static RoutingTable deserializeRoutingTable(PeerReference[] refs) {
        logger_.debug("Transforming CORBA PeerReference array to RoutingTable");
        RoutingTable rt = new RoutingTable();
        for (PeerReference serializedHost : refs) {
            PGridHost host = Deserializer.deserializePGridHost(serializedHost);
            rt.appendReference(host);
        }
        return rt;
    }

    /**
     * Transforms an {@link ExchangeInfo} object from the CORBA layer to a
     * {@link PGridPeer} object.
     *
     * @param info the object to be deserialized.
     * @return the {@link PGridPeer} object.
     */
    public static PGridPeer deserializeExchangeInfo(ExchangeInfo info) {
        logger_.info("Transforming CORBA ExchangeInfo to PGridPeer");
        return new PGridPeer(
                deserializePGridHost(info.peer),
                deserializeRoutingTable(info.routing));
    }
}
