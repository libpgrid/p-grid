package pgrid.communication.CORBAUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.communication.CORBAServices.PeerReference;
import pgrid.core.PGridHost;
import pgrid.core.RoutingTable;

import java.util.Collection;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Serializer {

    private static final Logger logger_ = LoggerFactory.getLogger(Serializer.class);

    /**
     * Serializes a {@link pgrid.core.PGridHost} object.
     *
     * @param host the host to be serialized.
     * @return the serialized result.
     */
    public static PeerReference serializePGridHost(PGridHost host) {
        logger_.debug("Transforming PGridHost to CORBA PeerReference");
        return new PeerReference(
                host.getAddress().getHostAddress(),
                host.getPort(),
                host.getHostPath().getPath(),
                host.getTimestamp(),
                host.getUUID().toString()
        );
    }

    /**
     * Serializes a {@link pgrid.core.RoutingTable} object.
     *
     * @param rt the routing table to be serialized.
     * @return the serialized result.
     */
    public static PeerReference[] serializeRoutingTable(RoutingTable rt) {
        logger_.debug("Transforming RoutingTable to CORBA PeerReference array");
        if (rt.length() == 0) {
            return new PeerReference[0];
        }

        Collection<PGridHost> list = rt.getTillLevel(rt.length() - 1);
        PeerReference[] refs = new PeerReference[rt.length()];
        int index = 0;
        for (PGridHost host : list) {
            refs[index] = serializePGridHost(host);
            index++;
        }
        return refs;
    }
}
