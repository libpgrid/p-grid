package pgrid.maintenance.repair;

import pgrid.core.PGridHost;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class RepairResult {
    private final PGridHost failedPeer_;
    private final PGridHost replacementPeer_;
    private final PGridHost conjugatePeer_;

    public RepairResult(PGridHost failed, PGridHost replacement, PGridHost conjugate) {
        failedPeer_ = failed;
        replacementPeer_ = replacement;
        conjugatePeer_ = conjugate;
    }

    public PGridHost getFailedPeer() {
        return failedPeer_;
    }

    public PGridHost getReplacement() {
        return replacementPeer_;
    }

    public PGridHost getConjugate() {
        return conjugatePeer_;
    }
}
