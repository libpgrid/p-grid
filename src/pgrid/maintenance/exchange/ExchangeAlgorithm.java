package pgrid.maintenance.exchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.core.PGridHost;
import pgrid.core.PGridPath;
import pgrid.core.RoutingTable;

/**
 * The class ExchangeAlgorithm encapsulates the exchange algorithm exactly as
 * described in the initial paper of P-Grid by Karl Aberer. The only precondition
 * for this class to execute properly is an {@link ExchangeContext} with
 * information about local and remote peer correctly initialized. That is the
 * method {@link pgrid.maintenance.exchange.ExchangeContext#isReadyForExchange()}
 * must return true.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExchangeAlgorithm {

    private static final Logger logger_ = LoggerFactory.getLogger(ExchangeAlgorithm.class);

    /**
     * Given ExchangeContext with local and remote peer information initialized,
     * it executes the exchange algorithm.
     *
     * @param context the context containing the exchange peer information.
     */
    public void execute(ExchangeContext context) {
        final RoutingTable localRoutingTable = context.getLocalRoutingTable();

        final PGridHost remoteHost = context.getRemoteHost();
        final PGridHost localHost = context.getLocalHost();

        logger_.info("Exchange algorithm beginning between local peer {} and remote peer {}",
                localHost, remoteHost);
        logger_.info("Local peer path: {} && Remote peer path: {}",
                localHost.getHostPath().getPath(), remoteHost.getHostPath().getPath());

        String commonPath = localHost.getHostPath().commonPrefix(remoteHost.getHostPath());
        int commonLength = commonPath.length();

        logger_.debug("Common path is {} with length {}", commonPath, commonLength);

        commonLength--; // sub for convenience

        if (commonLength < 0) {
            context.setCommonPathLength(commonLength + 1);
        } else {
            context.setCommonPathLength(commonLength);
        }

        // Exchanging references at the level where the paths agree.
        // Does not executes for the peers that are bootstrapping.
        localRoutingTable.update(commonLength, context.getRemoteRoutingTable());

        PGridPath localPath = localHost.getHostPath();
        PGridPath remotePath = remoteHost.getHostPath();
        int l1 = localPath.subPath(commonLength + 1, localPath.length()).length();
        int l2 = remotePath.subPath(commonLength + 1, remotePath.length()).length();

        if (l1 == 0 && l2 == 0) {
            // Case 1: if both remaining paths are empty introduce a new level.
            logger_.info("Case 1: (l1 == 0 && l2 == 0)");
            localPath.append(context.isInvited() ? '1' : '0');
            logger_.info("Appending {} to routing table.", remoteHost);
            localRoutingTable.appendReference(remoteHost);
        } else if (l1 == 0 && l2 > 0) {
            // Case 2: if one remaining path is empty split the shorter path.
            // When one of the peers is in this case, the other is in CASE *3*.
            logger_.info("Case 2: (l1 == 0 && l2 > 0)");
            localPath.revertAndAppend(remotePath.value(commonLength + 1));
            logger_.info("Appending {} to routing table.", remoteHost);
            localRoutingTable.appendReference(remoteHost);
        } else if (l1 > 0 && l2 == 0) {
            // Case 3: if one remaining path is empty split the shorter path.
            // When one of the peers is in this case, the other is in CASE *2*.
            logger_.info("Case 3: (l1 > 0 && l2 == 0)");
            localRoutingTable.updateWithRandom(commonLength + 1, remoteHost);
        } else if (l1 > 0 && l2 > 0) {
            // Case 4: recursively perform exchange with referenced peers.
            logger_.info("Case 4: (l1 > 0 && l2 > 0)");
            context.setRecursive();
        }

        logger_.info("[RESULT] Final path of local peer: {}", localPath.getPath());

    }

}
