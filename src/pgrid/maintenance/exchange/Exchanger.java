package pgrid.maintenance.exchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;
import pgrid.maintenance.Maintainer;
import pgrid.utilities.RunnableWorker;

import java.util.List;
import java.util.Vector;

/**
 * Exchanger is responsible for executing the exchange algorithm between the
 * local and a remote peer. Exchanges are done one at a time and only if the
 * remote peer has not changed its state.
 * <p/>
 * He can start an exchange process given a remote peer. The process consists
 * of the following steps:
 * <ol>
 * <li>
 * <b>1)</b> He will invite the remote peer. That peer will then answer true if he can
 * exchange, false otherwise.
 * </li>
 * <li>
 * <b>2)</b> If the first step returned true, then the Exchanger initiates by sending
 * information about the local peer accompanied with its routing table. In
 * return the remote peer answers with the same information.
 * </li>
 * <li>
 * <b>3)</b> From the second step, the Exchanger has all the information he needs to
 * execute the algorithm by calling
 * {@link ExchangeAlgorithm#execute(ExchangeContext)}
 * </li>
 * </ol>
 * <p/>
 * The Exchanger can execute an exchange process that a remote peer started.
 * It will execute only steps 2 and 3 as the invitation
 * <p/>
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public final class Exchanger extends RunnableWorker {

    private static final Logger logger_ = LoggerFactory.getLogger(Exchanger.class);

    // Holds the object that encapsulates the exchange algorithm.
    private final ExchangeAlgorithm exchange_ = new ExchangeAlgorithm();

    private final PGridProxy proxy_;
    private final PGridHost localhost_;
    private final RoutingTable localRoutingTable_;
    private final Maintainer maintainer_;

    // The list holds the hosts that the local peer wants to invite fo exchange.
    private final List<PGridHost> hostInvitation_ = new Vector<PGridHost>();

    // The list holds the hosts that have already invited the local peer.
    private final List<PGridPeer> requests_ = new Vector<PGridPeer>();

    private final Object requestLock_ = new Object();
    private final Object inviteLock_ = new Object();

    /**
     * Constructor.
     *
     * @param maintainer a reference to the {@link Maintainer} object.
     * @param proxy     a reference to a proxy able to make rpc to remote hosts.
     * @param localhost a reference to the {@link PGridHost} object that holds
     *                  information for the local peer.
     * @param localRT   a reference to the {@link RoutingTable} of the local peer.
     */
    public Exchanger(Maintainer maintainer, PGridProxy proxy, PGridHost localhost, RoutingTable localRT) {
        if (maintainer == null || proxy == null || localhost == null || localRT == null) {
            throw new NullPointerException();
        }

        maintainer_ = maintainer;
        proxy_ = proxy;
        localhost_ = localhost;
        localRoutingTable_ = localRT;
        logger_.debug("Exchanger for host {} is constructed", localhost_);
    }

    /**
     * Called when the local peer has accepted an invitation.
     *
     * @param host         the remote peer that invited the local peer.
     * @param routingTable the routing table of the remote peer.
     */
    public void addRequest(PGridHost host, RoutingTable routingTable) {
        synchronized (requestLock_) {
            requests_.add(new PGridPeer(host, routingTable));
        }
    }

    /**
     * Called when the local peer has accepted an invitation.
     *
     * @param peer the remote peer that invited the local peer.
     */
    public void addRequest(PGridPeer peer) {
        synchronized (requestLock_) {
            requests_.add(peer);
        }
    }

    /**
     * Called when the local peer wants to invite a remote peer for exchange.
     *
     * @param host the remote peer to be invited.
     */
    public void inviteHost(PGridHost host) {
        synchronized (inviteLock_) {
            hostInvitation_.add(host);
        }
    }

    /**
     * This method is the heart of this class. It checks if there are pending
     * requests from remote peers for exchange. Also it checks if the local
     * peer wants to invite other peers. In either case, this method dispatches
     * to the appropriate method to handle the two previous cases.
     */
    @Override
    public void workload() {
        PGridPeer[] peerRequests = null;
        synchronized (requestLock_) {
            if (!requests_.isEmpty()) {
                peerRequests = requests_.toArray(new PGridPeer[requests_.size()]);
                requests_.clear();
            }
        }

        if (peerRequests != null) {
            for (PGridPeer peer : peerRequests) {
                logger_.debug("{} requested an exchange.", peer.getHost());
                handleRequest(peer);
            }
        }

        PGridHost[] toInvite = null;
        synchronized (inviteLock_) {
            if (!hostInvitation_.isEmpty()) {
                toInvite = hostInvitation_.toArray(new PGridHost[hostInvitation_.size()]);
                hostInvitation_.clear();
            }
        }

        if (toInvite != null) {
            for (PGridHost host : toInvite) {
                logger_.debug("{} will be invited for exchange", host);
                handleInvitation(host);
            }
        }
    }

    /*
     ************************** Private Sector ********************************
     */

    /**
     * The local peer is invited and has accepted to execute the exchange
     * algorithm with a remote peer.
     * <p/>
     * When this method is executing by the local peer it means that the
     * remote peer, exchanging with the local, is executing the method
     * {@link Exchanger#handleInvitation(pgrid.core.PGridHost)}
     * Both Exchangers must block until those methods return.
     *
     * @param peerInfo holds all the remote peer information
     */
    private void handleRequest(PGridPeer peerInfo) {
        logger_.info("Handling request from peer {}", peerInfo.getHost());
        executeExchange(peerInfo, true);
    }

    /**
     * The exchanger will invite a remote peer to see if he is able to exchange.
     * If true then he initiate the process by swapping with the remote the
     * needed information (PGridHost and RoutingTable). Finally, when the
     * exchange context is ready it will execute the exchange algorithm.
     * <p/>
     * When this method is executing by the local peer it means that the
     * remote peer, exchanging with the local, is executing the method
     * {@link Exchanger#handleRequest(pgrid.core.PGridPeer)}
     * Both Exchangers must block until those methods return.
     *
     * @param host the host that the local host wants to exchange with.
     */
    private void handleInvitation(PGridHost host) {
        logger_.info("Exchange initiation with host {}", host.toString());
        PGridPeer remotePeer = proxy_.exchangeInfo(host, localhost_, localRoutingTable_);
        logger_.info("Got routing table of host {} with path {}", host, host.getHostPath());
        executeExchange(remotePeer, false);
    }

    /**
     * Executes the exchange algorithm for the given parameters.
     *
     * @param peerInfo  the remote peerInfo exchange information.
     * @param isInvited shows if the local peerInfo is invited or not.
     */
    private void executeExchange(PGridPeer peerInfo, boolean isInvited) {
        ExchangeContext context = new ExchangeContext(localhost_, localRoutingTable_, isInvited);
        context.setRemoteInfo(peerInfo.getHost(), peerInfo.getRoutingTable());
        if (context.isReadyForExchange()) {
            exchange_.execute(context);
        }
        logger_.info("{} completed exchange", localhost_.toString());
        peerInfo.getHost().exchanged();
        checkRecursion(context);
    }

    /**
     * After the exchange algorithm is executed, the {@link ExchangeContext}
     * must be checked to see if the host must execute exchange recursively.
     * <p/>
     * In case of a recursion it will add the hosts kept in the context and
     * will exchange with them with the first opportunity.
     *
     * @param context the context that was passed to
     *                {@link ExchangeAlgorithm#execute(ExchangeContext)}.
     */
    private void checkRecursion(ExchangeContext context) {
        if (context.isRecursive()) {
            logger_.info("Recursion");
            int common = context.getCommonPathLength();
            RoutingTable remoteRoutingTable = context.getRemoteRoutingTable();
            maintainer_.randomExchange(remoteRoutingTable.getAllLevels());
        }
    }
}
