package pgrid.maintenance.exchange;

import pgrid.core.PGridHost;
import pgrid.core.RoutingTable;

/**
 * This is a simple class that stores information needed for exchange. Objects
 * of this class are used only inside pgrid.maintenance.exchange package.
 * The {@link Exchanger} will construct such objects and pass them when they
 * have all the wanted information to the {@link ExchangeAlgorithm}.
 * <p/>
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExchangeContext {

    private final PGridHost localHost_;
    private final RoutingTable localRoutingTable_;

    private PGridHost remoteHost_ = null;
    private RoutingTable remoteRoutingTable_ = null;

    private boolean invited_;

    private boolean recursive_;
    private int commonPathLength_;

    private boolean readyForExchange_;

    /**
     * Constructor.
     *
     * @param localHost a reference to the local peer.
     * @param localRT   a reference to the routing table of the local peer.
     * @param invited   a flag that shows if the local peer was invited or not.
     */
    public ExchangeContext(PGridHost localHost, RoutingTable localRT, boolean invited) {
        if (localHost == null || localRT == null) {
            throw new NullPointerException("The given method arguments are null.");
        }

        localHost_ = localHost;
        localRoutingTable_ = localRT;
        invited_ = invited;
        recursive_ = false;
        commonPathLength_ = 0;
        readyForExchange_ = false;
    }

    /**
     * Validates the context and sets the ready-for-exchange flag.
     */
    private void validateContext() {
        if (remoteHost_ != null && remoteRoutingTable_ != null) {
            readyForExchange_ = true;
        }
    }

    /**
     * Returns if the current context is ready for exchange.
     *
     * @return true if local and remote peer information are all set.
     */
    public boolean isReadyForExchange() {
        validateContext();
        return readyForExchange_;
    }

    /**
     * It sets information about the remote peer to be exchanged with.
     *
     * @param remoteHost the remote peer.
     * @param remoteRT   the routing table of the remote peer.
     */
    public void setRemoteInfo(PGridHost remoteHost, RoutingTable remoteRT) {
        if (remoteHost == null || remoteRT == null) {
            throw new NullPointerException("The given method arguments are null.");
        }

        remoteHost_ = remoteHost;
        remoteRoutingTable_ = remoteRT;
        readyForExchange_ = true;
    }

    /**
     * Returns the routing table of the remote peer.
     *
     * @return the routing table.
     */
    public RoutingTable getRemoteRoutingTable() {
        return remoteRoutingTable_;
    }

    /**
     * Returns the routing table of the local peer.
     *
     * @return the routing table.
     */
    public RoutingTable getLocalRoutingTable() {
        return localRoutingTable_;
    }

    /**
     * Returns the {@link PGridHost} object associated with the remote peer.
     *
     * @return the remote peer.
     */
    public PGridHost getRemoteHost() {
        return remoteHost_;
    }

    /**
     * Returns the {@link PGridHost} object associated with the local peer.
     *
     * @return the local peer.
     */
    public PGridHost getLocalHost() {
        return localHost_;
    }

    /**
     * Tells if the local peer was invited by the remote or not.
     *
     * @return true if the local peer was invited and false otherwise.
     */
    public boolean isInvited() {
        return invited_;
    }

    /**
     * It sets the common path length between the two peers.
     *
     * @param length the common path length.
     */
    public void setCommonPathLength(int length) {
        commonPathLength_ = length;
    }

    /**
     * It will return the common path length between the two peers.
     *
     * @return the common path length.
     */
    public int getCommonPathLength() {
        return commonPathLength_;
    }

    /**
     * If the exchange algorithm ended with the case of recursion, it will
     * inform the exchange context.
     */
    public void setRecursive() {
        recursive_ = true;
    }

    /**
     * Tells if there was the recursion case during the algorithm execution.
     *
     * @return true in case of recursion.
     */
    public boolean isRecursive() {
        return recursive_;
    }
}
