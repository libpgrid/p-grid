package pgrid.maintenance.exchange;

import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;

import java.util.List;

/**
 * Idea: Periodically run this task with an {@link java.util.concurrent.Executor}.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExchangeTask implements Runnable {
    private final RoutingTable localRT_;
    private final PGridHost localHost_;
    private final PGridProxy proxy_;
    private final ExchangeAlgorithm exchange_;

    public ExchangeTask(PGridProxy proxy, RoutingTable localRT, PGridHost localHost) {
        proxy_ = proxy;
        localRT_ = localRT;
        localHost_ = localHost;
        exchange_ = new ExchangeAlgorithm();
    }

    @Override
    public void run() {
        //PGridHost remoteHost = localRT_.getRandomLevel();
        PGridHost remoteHost = selectHost();
        boolean isOnline = proxy_.isOnline(remoteHost.getAddress(), remoteHost.getPort());
        if (isOnline) {
            PGridPeer peerInfo = proxy_.exchangeInfo(remoteHost, localHost_, localRT_);
            ExchangeContext context = new ExchangeContext(localHost_, localRT_, false);
            context.setRemoteInfo(peerInfo.getHost(), peerInfo.getRoutingTable());
            exchange_.execute(context);
        } else {
            // send to maintainer
        }
    }

    private PGridHost selectHost() {
        List<PGridHost> refs = (List<PGridHost>) localRT_.getAllLevels();
        PGridHost minTime = refs.get(0);
        for (PGridHost host : refs) {
            if (host.getTimestamp() < minTime.getTimestamp()) {
                minTime = host;
            }
        }
        return minTime;
    }
}
