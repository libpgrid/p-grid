package pgrid.maintenance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;
import pgrid.maintenance.exchange.Exchanger;
import pgrid.utilities.RunnableWorker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class Maintainer extends RunnableWorker {

    public enum State {
        INITIALIZE,
        BOOTSTRAP,
        WORK,
        REPAIR
    }

    private static final Logger logger_ = LoggerFactory.getLogger(Maintainer.class);

    private final PGridProxy proxy_;
    private final PGridHost localHost_;
    private final RoutingTable localRT_;

    private Exchanger exchanger_;
    private Thread exchangerThread_;

    private State maintenanceState_;

    private Queue<PGridHost> queue_;

    //******* Initialization methods including factory method *********************//
    public static Maintainer createMaintainer(PGridP2P reference) {
        Maintainer object = new Maintainer(reference);
        object.initializeServices(reference.isBootstrap());
        return object;
    }

    protected Maintainer(PGridP2P reference) {
        proxy_ = reference.getServiceProxy();
        localHost_ = reference.getLocalHost();
        localRT_ = reference.getLocalRoutingTable();
        queue_ = new ConcurrentLinkedQueue<PGridHost>();

        maintenanceState_ = State.INITIALIZE;
    }

    private void initializeServices(boolean isBootstrap) {
        logger_.info("Starting maintenance services.");
        logger_.info("Starting exchanger service.");
        exchanger_ = new Exchanger(this, proxy_, localHost_, localRT_);
        exchangerThread_ = new Thread(exchanger_, "Thread-Exchanger");
        exchangerThread_.setDaemon(true);
        exchangerThread_.start();
        logger_.info("Maintenance services started successfully.");

        // FIXME: temp hack to define bootstrapper
        if (isBootstrap) {
            maintenanceState_ = State.WORK;
        } else {
            maintenanceState_ = State.BOOTSTRAP;
        }
    }
    //*****************************************************************************//

    public void stopServices() {
        logger_.info("Shutting down maintenance services.");
        exchanger_.cancel();
        try {
            exchangerThread_.join(); // after this the thread is in terminated state
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger_.debug("Exchanger thread state: {}", exchangerThread_.getState());
        logger_.debug("Exchanger thread isAlive? {}", exchangerThread_.isAlive());

        super.cancel();
        logger_.debug("Maintainer will stop.");
    }

    public String maintenanceState() {
        return maintenanceState_.name();
    }


    @Override
    public void workload() {
        if (maintenanceState_.equals(State.WORK)) {
            if (localRT_.length() > 0) {
                PGridHost toInvite = localRT_.selectExchangeHost();
                if (toInvite != null) {
                    logger_.info("Selected {} to invite for exchange.", toInvite);
                    toInvite.exchangeInitiated();
                    exchange(toInvite);
                }
            }
        }
    }

    /*
    **************** Maintenance Operations ****************************************************************
    */

    public void join(PGridHost remoteHost) {
        logger_.debug("Local peer is bootstrapping");
        maintenanceState_ = State.BOOTSTRAP;

        RoutingTable remoteRT = proxy_.getRemoteRT(remoteHost.getAddress().getHostName(), remoteHost.getPort());
        List<PGridHost> bootstrappers = new ArrayList<PGridHost>();
        bootstrappers.add(remoteHost);
        bootstrappers.addAll(remoteRT.getAllLevels());
        bootstrap(bootstrappers);

        while (localRT_.length() == 0) {}
        maintenanceState_ = State.WORK;
        logger_.debug("Local peer in WORK state now.");
    }

    public void bootstrap(Collection<PGridHost> hosts) {
        for (PGridHost remoteHost : hosts) {
            if (proxy_.isOnline(remoteHost.getAddress(), remoteHost.getPort())) {
                if (State.valueOf(proxy_.maintenanceState(remoteHost)).equals(State.WORK)) {
                    logger_.info("Bootstrapping with host {}", remoteHost);
                    exchanger_.inviteHost(remoteHost);
                }
            }
        }
    }

    public void randomExchange(Collection<PGridHost> hosts) {
            logger_.info("Hosts to exchange with: {}", hosts);
            for (PGridHost host : hosts) {
                if (host.compareTo(localHost_) == 0) {
                    logger_.info("Host {} is me actually.", host);
                    //continue;
                } else  if (localRT_.containsHost(host)) {
                    logger_.info("This host {} is in my routing table", host);
                    //continue;
                } else {
                    logger_.info("Informing Maintainer to start exchange with {}", host);
                    exchange(host);
                }
            }
    }
    
    /**
     * @param remoteHost
     */
    public void exchange(PGridHost remoteHost) {
        logger_.info("Initiating exchange with {}.", remoteHost);
        // 1) Check if the selected peed is online.
        if (proxy_.isOnline(remoteHost.getAddress(), remoteHost.getPort())) {
            logger_.info("{} is online", remoteHost.toString());
            // 2.a) If the peer is online then check his state
            switch (State.valueOf(proxy_.maintenanceState(remoteHost))) {
                case WORK:
                    logger_.info("Remote peer {} is working.", remoteHost);
                    // 2.a.i) If the remote state is WORK then execute exchange.
                    exchanger_.inviteHost(remoteHost);
                    break;
                case REPAIR:
                    logger_.info("Remote peer {} is repairing.", remoteHost);
                    // 2.a.ii) If the remote state is REPAIR then ask for the failed peer
                    PGridHost failed = proxy_.getFailedPeer(remoteHost);
                    logger_.info("Peer {} has failed.", failed);
                    // Check if the peer is in my routing table
                    if (localRT_.containsHost(failed)) {
                        logger_.info("Local routing table contains failed peer {}.", failed);
                        // If yes then register as a listener to the remote peer goto 1
                        proxy_.waitingSolution(remoteHost, localHost_);
                    }
                    break;
                default:
                    logger_.info("Remote peer {} is not in a state working state yet.", remoteHost);
                    break;
            }
        } else {
            logger_.debug("{} is not online.", remoteHost);
            // 2.b) If the peer is offline then enter REPAIR state
            //maintenanceState_ = State.REPAIR;
            // Execute repair
            //repair(remoteHost);
        }
    }

    public void repair(PGridHost remoteHost) {
        logger_.info("Initiating repair for failed peer {}.", remoteHost);
    }

    /* CALLBACKS */
    public void exchangeRequest(PGridPeer remotePeer) {
        // Issued by PGridEndpoint
        logger_.debug("local UUID {}.", localHost_.getUUID());

        if (remotePeer.getHost().compareTo(localHost_) == 0) {
            logger_.info("Exchange not continued since the remote is the local host.");
            return;
        }

        if (maintenanceState_.equals(State.REPAIR)) {
            // TODO: implement logic
        }

        logger_.info("Passing peer {} to Exchanger.", remotePeer.getHost());
        exchanger_.addRequest(remotePeer);
    }

    public void repairRequest(PGridHost remoteHost) {
        // Issued by PGridEndpoint
        // Repair
    }

    public void bootstrapRequest(PGridHost remoteHost) {
        // Issued by PGridEndpoint
        // Bootstrap
    }
}
