package pgrid.core;

import java.util.concurrent.atomic.AtomicLong;


/**
 * LamportClock is an implementation of the Lamport clocks as described in his
 * paper.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public final class LamportClock implements PeerClock<Long> {

    private final AtomicLong timestamp_;

    public LamportClock() {
        timestamp_ = new AtomicLong(0);
    }

    /**
     * Resets the clock to its initial value '0'.
     */
    @Override
    public void reset() {
        timestamp_.set(0);
    }

    /**
     * This method must be used when a message is about to be sent.
     *
     * @return The current timestamp.
     */
    @Override
    public Long getTimestamp() {
        return timestamp_.get();
    }

    /**
     * When a message is received we must increase the local timestamp and then
     * compare it to the remote. Finally, choose the maximum one and set it to
     * local clock.
     *
     * @param timestamp This is the timestamp of the remote peer the moment it
     *                  sent the message.
     */
    @Override
    public void clockedEvent(Long timestamp) {
        Long next = Math.max(timestamp, timestamp_.get());
        next++;
        timestamp_.set(next);
    }
}
