package pgrid.core;

import java.io.FileNotFoundException;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public abstract class XMLPersistentRoutingTable implements PersistentRoutingTable {

    @Override
    public void load(String filename) throws FileNotFoundException {

    }

    @Override
    public void store(String filename) throws FileNotFoundException {

    }
}
