package pgrid.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.utilities.IOUtilities;

import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

// FIXME: Synchronize methods.

/**
 * This class represents the routing table of the peer and stores pairs
 * containing a {@link pgrid.core.PGridHost} associated with a level.
 * A level is a path of the P-Grid tree. We don't care what is the path and so
 * a level takes integer values beginning from zero (0). Independent of path
 * schematics, the path can be found by considering the level number and the
 * path of this peer holding this routing table. For example, if the peer has
 * the path "01", then level 0 stores a host associated with path "1". Finally,
 * level 1 stores a host responsible for path "00".
 * <p/>
 * <b>NOTE</b>: Assumption that refMax = 1 in contrast to the paper where this
 * can vary. These are the fidgets, different hosts stored for the same path.
 * <p/>
 * TODO: Extract an interface for basic routing table operations.
 * TODO: How to separate clearly persistency and basic operations?
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class RoutingTable implements PersistentRoutingTable {

    private static final Logger logger_ = LoggerFactory.getLogger(RoutingTable.class);

    private final List<PGridHost> references_ = new Vector<PGridHost>();
    private final Map<UUID, PGridHost> uuidRefs_ = new ConcurrentHashMap<UUID, PGridHost>();
    private final List<PGridHost> fidgets_ = new Vector<PGridHost>();
    
    // variables used to turn this routing table into an XML file.
    private static final String ROOT = "RoutingTable";
    private static final String HOST = "host";
    private static final String LEVEL = "level";
    private static final String ADDRESS = "address";
    private static final String PORT = "port";
    private static final String PATH = "path";
    private static final String UUID_STR = "uuid";
    private static final String TIMESTAMP = "timestamp";

    /**
     * Default constructor.
     */
    public RoutingTable() {
    }

    public void addFidget(PGridHost host) {
        fidgets_.add(host);
    }

    public List<PGridHost> getFidgets() {
        return fidgets_;
    }

    /**
     * It updates this routing table with random references taken from this and
     * the remote routing table till the level where the path agree.
     * This method is used during the exchange algorithm.
     *
     * @param pathLvl  the level where the paths agree.
     * @param remoteRT the routing table of a remote host.
     */
    public synchronized void update(int pathLvl, RoutingTable remoteRT) {
        if (remoteRT == null) {
            throw new NullPointerException();
        } else if (pathLvl <= 0) {
            return;
        }

        logger_.info("Updating local routing table with remote till path level {}", pathLvl);
        Collection<PGridHost> commonRefs =
                union(this.getTillLevel(pathLvl), remoteRT.getTillLevel(pathLvl));
        updateReferences(randomSelect(pathLvl + 1, commonRefs));
    }

    /**
     * Inserts an additional level at the end of the routing table and maps it
     * with the given PGridHost reference.
     *
     * @param host the reference to be associated with the new level.
     * @throws NullPointerException if the host is null.
     */
    public synchronized void appendReference(PGridHost host) {
        if (host == null) {
            throw new NullPointerException();
        }
        references_.add(host);
        uuidRefs_.put(host.getUUID(), host);
    }

    /**
     * Removes the last level of the routing table and returns that PGridHost.
     * It will return true if the routing table performed successfully the
     * removal. This operations returns false when the table is of size
     * zero (0).
     *
     * @return true if the last level was removed as a result of this call.
     */
    public synchronized boolean removeLastReference() {
        boolean result = false;
        if (!references_.isEmpty()) {
            int index = references_.size() - 1;
            result = true;
            PGridHost host = references_.remove(index);
            uuidRefs_.remove(host.getUUID());
        }
        return result;
    }

    /**
     * Given a level of the routing table, it updates the reference with a
     * given host.
     *
     * @param pathLevel the level that the replacement will take place.
     * @param host      the new host.
     * @throws IllegalArgumentException in case of a pathLevel not defined in
     *                                  the routing table.
     * @throws NullPointerException     if the host is null.
     */
    public synchronized void updateReference(int pathLevel, PGridHost host) {
        if (pathLevel < 0 || pathLevel >= references_.size()) {
            throw new IllegalArgumentException("The specified path is illegal");
        } else if (host == null) {
            throw new NullPointerException();
        }

        PGridHost toUpdate = references_.get(pathLevel);
        uuidRefs_.remove(toUpdate.getUUID());
        uuidRefs_.put(host.getUUID(), host);
        references_.set(pathLevel, host);
    }

    /**
     * Given a collection of hosts, it updates the references of this routing
     * table from its start till the length of the collection. In case the
     * length of the collection is longer that that of the routing table, it
     * inserts only till the level of the table.
     *
     * @param hosts to update the routing table.
     */
    public synchronized void updateReferences(Collection<PGridHost> hosts) {
        if (hosts == null) {
            throw new NullPointerException();
        }
        int i = 0;
        for (PGridHost host : hosts) {
            updateReference(i, host);
            i++;
            if (i >= references_.size()) {
                break;
            }
        }
    }

    /**
     * It selects a random host from the hosts stored in this routing table till
     * the specified path level with the host passed as method argument. It then
     * updates the reference of this routing table at the specified path level.
     *
     * @param pathLevel the path level where the update will happen.
     * @param host      the host to be added in the set when randomly selecting.
     */
    public synchronized void updateWithRandom(int pathLevel, PGridHost host) {
        if (host == null) {
            throw new NullPointerException();
        }

        if (pathLevel > references_.size()) {
            return;
        }
        
        PGridHost leveledHost= getLevel(pathLevel);
        Collection<PGridHost> coll = new ArrayList<PGridHost>(2);
        coll.add(leveledHost);
        coll.add(host);
        List<PGridHost> list = (List<PGridHost>) randomSelect(1, coll);
        logger_.info("Update for level {}, from {} to {}", new Object[]{pathLevel, getLevel(pathLevel), list.get(0)});
        updateReference(pathLevel, list.get(0));
    }

    /**
     * If this routing table contains the given host by looking up his UUID, it
     * will update the information about him. If he doesn't exist then it will
     * do nothing.
     *
     * @param host the host to look up.
     */
    public synchronized void updateHost(PGridHost host) {
        if (uuidRefs_.containsKey(host.getUUID())) {
            PGridHost toUpdate = uuidRefs_.get(host.getUUID());
            toUpdate.setHostPath(host.getHostPath().getPath());
            toUpdate.setTimestamp(host.getTimestamp());
        }
    }

    /**
     * It returns all the hosts that are stored in this routing table.
     *
     * @return a Collection including all hosts stored in the table.
     */
    public synchronized Collection<PGridHost> getAllLevels() {
        return references_;
    }

    /**
     * Given a path, it returns in a Collection all the hosts associated with
     * the levels starting from level zero (0) and ending at level pathLevel.
     * Note that pathLevel IS included in the returned Collection.
     *
     * @param pathLevel the level up to where it will look.
     * @return a Collection with all the references until the specified level.
     * @throws IllegalArgumentException in case of a pathLevel not defined in
     *                                  the routing table.
     */
    public synchronized Collection<PGridHost> getTillLevel(int pathLevel) {
        if (pathLevel < 0 || pathLevel >= references_.size()) {
            throw new IllegalArgumentException("The specified path is illegal");
        }

        Collection<PGridHost> list = new Vector<PGridHost>(pathLevel + 1);
        for (int i = 0; i <= pathLevel; i++) {
            list.add(references_.get(i));
        }

        return list;
    }

    /**
     * Returns the reference associated with a given path level.
     *
     * @param pathLevel the path level that is associated with a host.
     * @return the PGridHost.
     * @throws IllegalArgumentException in case of a pathLevel not defined in
     *                                  the routing table.
     */
    public synchronized PGridHost getLevel(int pathLevel) {
        if (pathLevel < 0 || pathLevel >= references_.size()) {
            throw new IllegalArgumentException("The specified path is illegal");
        }

        return references_.get(pathLevel);
    }

    /**
     * Selects a random host from those stored in the routing table.
     *
     * @return the random host.
     */
    public synchronized PGridHost getRandomLevel() {
        Random r = new Random();
        r.setSeed(System.currentTimeMillis());
        int randLevel = r.nextInt(references_.size());
        return getLevel(randLevel);
    }

    /**
     * Clears the routing table by removing all the levels and all the hosts
     * associated with them.
     */
    public synchronized void clear() {
        references_.clear();
        uuidRefs_.clear();
    }

    /**
     * The length of the routing table is actually the maximum path stored in
     * it.
     *
     * @return the length of the routing table.
     */
    public synchronized int length() {
        return references_.size();
    }

    /**
     * Checks if the given host exists in the routing table by comparing its
     * UUID.
     *
     * @param host the host to be checked.
     * @return true if the hosts exists, false else.
     */
    public synchronized boolean containsHost(PGridHost host) {
        return uuidRefs_.containsKey(host.getUUID());
    }

    /**
     * Given a UUID it will return the associated host if there is one with
     * such a UUID in the routing table, else it will return a null value.
     *
     * @param uuid the UUID to be searched.
     * @return the host with that UUID.
     */
    public synchronized PGridHost selectUUIDHost(UUID uuid) {
        return uuidRefs_.get(uuid);
    }

    /**
     * Selects a random host from this routing table.
     *
     * @return the random host.
     */
    public synchronized PGridHost selectRandomHost() {
        Random r = new Random();
        r.setSeed(System.currentTimeMillis());
        if (references_.size() == 0) {
            return references_.get(0);
        }
        return references_.get(r.nextInt(references_.size())); // [0, size)
    }

    /**
     * Considering the timestamp of every host stored in this routing table, it
     * selects and returns the oldest.
     *
     * @return the oldest host.
     */
    public synchronized PGridHost selectOldestHost() {
        PGridHost minTS = references_.get(0);
        for (PGridHost host : references_) {
            if (host.getTimestamp() < minTS.getTimestamp()) {
                minTS = host;
            }
        }
        return minTS;
    }

    /**
     * Given a low timestamp bound, it returns the first host it finds with
     * a timestamp lower than the bound. If there isn't such a host in the
     * routing table it will return a null value.
     *
     * @param minBound the timestamp bound.
     * @return the host or null if there's not such a host..
     */
    public synchronized PGridHost selectBoundedHost(long minBound) {
        PGridHost minTS = null;
        for (PGridHost host : references_) {
            if (host.getTimestamp() < minBound) {
                minTS = host;
                break;
            }
        }
        return minTS;
    }

    public synchronized PGridHost selectExchangeHost() {
        PGridHost forExchange = null;
        for (PGridHost host : references_) {
            if (host.isExchangeTime()) {
                forExchange = host;
                break;
            }
        }
        return forExchange;
    }

    /**
     * Given two different collections containing PGridHost references, it
     * performs the union. The result contains all the hosts with different
     * UUIDs, sorted on their paths.
     *
     * @param refs1 the first PGridHost Collection.
     * @param refs2 the second PGridHost Collection.
     * @return the union of the two collections.
     */
    public static Collection<PGridHost> union(Collection<PGridHost> refs1, Collection<PGridHost> refs2) {
        if (refs1 == null || refs2 == null) {
            throw new NullPointerException();
        }

        // 1) Filter on UUIDs.
        Collection<PGridHost> setUUID = new TreeSet<PGridHost>();
        setUUID.addAll(refs1);
        setUUID.addAll(refs2);

        // 2) Filter and sort on PGridPath.
        Collection<PGridHost> setPath = Collections.synchronizedSet(new TreeSet<PGridHost>(
                new Comparator<PGridHost>() {
                    @Override
                    public int compare(PGridHost host1, PGridHost host2) {
                        int result;
                        String host1Path = host1.getHostPath().getPath();
                        String host2Path = host2.getHostPath().getPath();
                        if (host1Path.length() == host2Path.length()) {
                            result = host1Path.compareTo(host2Path);
                        } else if (host1Path.length() < host2Path.length()) {
                            result =  -1;
                        } else  { // if (host1Path.length() < host2Path.length()) {
                            result =  +1;
                        }
//                        } else {
//                            result = -host1Path.compareTo(host2Path);
//                        }
                        return result;
                    }
                }));
        setPath.addAll(setUUID);

        return setPath;
    }

    /**
     * Given a Collection with PGridHost references, it returns a random
     * subset with refMax elements.
     *
     * @param refMax     the size of the random subset.
     * @param commonRefs the Collection where the selection will be performed.
     * @return a Collection with the random subset.
     */
    public static Collection<PGridHost> randomSelect(int refMax, Collection<PGridHost> commonRefs) {
        if (commonRefs.size() <= refMax) {
            return commonRefs;
        }

        List<PGridHost> copy = new Vector<PGridHost>(commonRefs);
        Collections.shuffle(copy);
        return copy.subList(0, refMax);
    }

    @Override
    public synchronized void load(String file) throws FileNotFoundException {
        // TODO: Break this methods into simpler (REFACTOR)
        InputStream inStream = null;

        try {
            Map<Integer, PGridHost> map = new HashMap<Integer, PGridHost>();
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            inStream = new FileInputStream(file);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(inStream);

            PGridHost host;

            int level = -1;
            String address = null;
            int port = -1;
            String path = "";
            String uuid = "";
            long ts = -1;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    if (startElement.getName().getLocalPart().equals(HOST)) {
                        Iterator attributes = startElement.getAttributes();
                        while (attributes.hasNext()) {
                            Attribute attribute = (Attribute) attributes.next();
                            if (attribute.getName().toString().equals(LEVEL)) {
                                level = Integer.parseInt(attribute.getValue());
                            }

                        }
                    }

                    if (event.isStartElement()) {
                        if (event.asStartElement().getName().getLocalPart().equals(ADDRESS)) {
                            event = eventReader.nextEvent();
                            address = event.asCharacters().getData();
                            continue;
                        }
                    }
                    if (event.asStartElement().getName().getLocalPart().equals(PORT)) {
                        event = eventReader.nextEvent();
                        port = Integer.parseInt(event.asCharacters().getData());
                        continue;
                    }

                    if (event.asStartElement().getName().getLocalPart()
                            .equals(PATH)) {
                        event = eventReader.nextEvent();
                        path = event.asCharacters().getData();
                        continue;
                    }

                    if (event.asStartElement().getName().getLocalPart()
                            .equals(UUID_STR)) {
                        event = eventReader.nextEvent();
                        uuid = event.asCharacters().getData();
                        continue;
                    }

                    if (event.asStartElement().getName().getLocalPart()
                            .equals(TIMESTAMP)) {
                        event = eventReader.nextEvent();
                        ts = Integer.parseInt(event.asCharacters().getData());
                        continue;
                    }
                }

                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals(HOST)) {
                        host = new PGridHost(address, port);
                        host.setHostPath(path);
                        host.setUUID(UUID.fromString(uuid));
                        host.setTimestamp(ts);
                        map.put(level, host);

                        level = -1;
                        address = null;
                        port = -1;
                        path = "";
                        uuid = "";
                        ts = -1;
                    }
                }
            }
            for (Map.Entry<Integer, PGridHost> entry : map.entrySet()) {
                appendReference(entry.getValue());
            }
        } catch (UnknownHostException e) {
            logger_.error("{}", e);
        } catch (XMLStreamException e) {
            logger_.error("{}", e);
        } finally {
            IOUtilities.closeQuietly(inStream);
        }
    }

    @Override
    public synchronized void store(String filename) throws FileNotFoundException {
        // TODO: Break this methods into simpler (REFACTOR)
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(filename);

            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
            XMLEventWriter eventWriter = outputFactory
                    .createXMLEventWriter(outStream);

            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
            XMLEvent end = eventFactory.createDTD("\n");
            XMLEvent tab = eventFactory.createDTD("\t");

            StartDocument startDocument = eventFactory.createStartDocument();
            eventWriter.add(startDocument);
            eventWriter.add(end);

            StartElement rtStartElement = eventFactory.createStartElement("",
                    "", ROOT);
            eventWriter.add(rtStartElement);
            eventWriter.add(end);

            String doubleTab = "\t\t";
            int level = 0;
            for (PGridHost host : getAllLevels()) {
                StartElement hostStartElement = eventFactory.createStartElement("", "", HOST);
                Attribute attribute = eventFactory.createAttribute(LEVEL, Integer.toString(level));
                eventWriter.add(tab);
                eventWriter.add(hostStartElement);
                eventWriter.add(attribute);
                eventWriter.add(end);

                createNode(eventWriter, doubleTab, ADDRESS, host.getAddress().getHostName());
                createNode(eventWriter, doubleTab, PORT, Integer.toString(host.getPort()));
                createNode(eventWriter, doubleTab, PATH, host.getHostPath().toString());
                createNode(eventWriter, doubleTab, TIMESTAMP, Long.toString(host.getTimestamp()));
                createNode(eventWriter, doubleTab, UUID_STR, host.getUUID().toString());

                eventWriter.add(tab);
                eventWriter.add(eventFactory.createEndElement("", "", HOST));
                eventWriter.add(end);
                level++;
            }

            eventWriter.add(eventFactory.createEndElement("", "", ROOT));
            eventWriter.add(end);
            eventWriter.add(eventFactory.createEndDocument());
            eventWriter.close();
        } catch (XMLStreamException e) {
            logger_.error("{}", e);
        } finally {
            IOUtilities.closeQuietly(outStream);
        }
    }

    private void createNode(XMLEventWriter eventWriter, String indent, String name, String value)
            throws XMLStreamException {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent end = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD(indent);
        // Create Start node
        StartElement sElement = eventFactory.createStartElement("", "", name);
        eventWriter.add(tab);
        eventWriter.add(sElement);
        // Create Content
        Characters characters = eventFactory.createCharacters(value);
        eventWriter.add(characters);
        // Create End node
        EndElement eElement = eventFactory.createEndElement("", "", name);
        eventWriter.add(eElement);
        eventWriter.add(end);

    }
}
