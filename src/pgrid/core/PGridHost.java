package pgrid.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

/**
 * Information about a peer of the p-grid network are stored here. The IP, port,
 * of the peer so the local one knows where to talk. Also there's the path of
 * the p-grid tree that the peer is responsible. Finally, there's a timestamp
 * that shows how new the information stored here is. Every peer holds locally
 * a clock and when messages arrive, it can compare the timestamps and act
 * accordingly.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridHost implements Comparable<PGridHost> {

    private static final Logger logger_ = LoggerFactory.getLogger(PGridHost.class);

    private InetAddress address_;
    private int port_;
    private final PGridPath path_ = new PGridPath();
    private LamportClock clock_ = new LamportClock();
    private UUID uuid_ = UUID.randomUUID();

    private static final int MIN_EXCHANGE_DELAY = 1000 * 1;
    private long exchangeTime_ = 0;
    
    /**
     * Constructs a PGridHost.
     *
     * @param ip   The IP address of the host.
     * @param port The port of the host.
     * @throws UnknownHostException     if the given IP could not be determined.
     * @throws IllegalArgumentException in case the port argument is negative.
     */
    public PGridHost(String ip, int port) throws UnknownHostException {
        if (port <= 0) {
            throw new IllegalArgumentException(
                    "Not a valid port (port = " + port + ")");
        }

        address_ = InetAddress.getByName(ip);
        port_ = port;
    }

    /**
     * Constructs a PGridHost.
     *
     * @param ip   The IP address of the host encapsulated in an InetAddress.
     * @param port The port of the host.
     * @throws IllegalArgumentException in case the port argument is negative.
     */
    public PGridHost(InetAddress ip, int port) {
        if (port <= 0) {
            throw new IllegalArgumentException(
                    "Not a valid port (port = " + port + ")");
        }

        address_ = ip;
        port_ = port;
    }

    /**
     * Convert this PGridHost to a String. It returns a string of the form
     * "IP:port".
     *
     * @return a string representation of this host.
     */
    @Override
    public String toString() {
        return address_.getHostName();//address_.getHostAddress() + ":" + port_;
    }

    /**
     * @return the InetAddress of the host.
     */
    public InetAddress getAddress() {
        return address_;
    }

    /**
     * Sets a new address for this host.
     *
     * @param address the new address of this host.
     */
    public void setAddress(InetAddress address) {
        address_ = address;
    }

    // FIXME: setHostPath(String path) is redundant
    public void setHostPath(String path) {
        path_.setPath(path);
    }

    /**
     * @return the path of this host.
     */
    // FIXME: getHostPath() is redundant
    public PGridPath getHostPath() {
        return path_;
    }

    /**
     * @return the port of this host.
     */
    public int getPort() {
        return port_;
    }

    /**
     * Updates the port of this host.
     *
     * @param port the new port of this host.
     */
    public void setPort(int port) {
        port_ = port;
    }

    /**
     * The timestamp when compared to the clock of the local peer, reveals how
     * synchronized and updated the information stored here is. If it's old
     * then some steps must be followed to update it.
     *
     * @return a timestamp
     */
    public long getTimestamp() {
        return clock_.getTimestamp();
    }

    /**
     * Updates the timestamp of this host. This means that the local peer has
     * new information about this peer obtained through messaging.
     *
     * @param timestamp that will update the old one.
     */
    public void setTimestamp(long timestamp) {
        clock_.clockedEvent(timestamp);
    }

    /**
     * Resets the timestamp for this host.
     *
     * @see LamportClock
     */
    public void resetTimestamp() {
        clock_.reset();
    }

    /**
     * Returns the UUID of this host.
     *
     * @return the UUID.
     */
    public UUID getUUID() {
        return uuid_;
    }

    /**
     * Sets the UUID for this host.
     *
     * @param uuid the new UUID.
     */
    public void setUUID(UUID uuid) {
        uuid_ = uuid;
    }

    /**
     * {@inheritDoc}
     * The comparison is done based on the UUIDs of the hosts.
     */
    @Override
    public int compareTo(PGridHost host) {
        return getUUID().compareTo(host.getUUID());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (!o.getClass().equals(this.getClass()))
            return false;

        return getUUID().equals(((PGridHost) o).getUUID());
    }

    public int hashCode() {
        assert false : "hashCode not designed";
        return 42; // the answer to all.
    }

    public boolean isExchangeTime() {
        return (exchangeTime_ < System.currentTimeMillis());
    }

    public void exchangeInitiated() {
        exchangeTime_ = System.currentTimeMillis() + MIN_EXCHANGE_DELAY / 2;
    }

    public void exchanged() {
        logger_.debug("This host {} has just completed exchanging", toString());
        exchangeTime_ = System.currentTimeMillis() + MIN_EXCHANGE_DELAY;
    }
}
