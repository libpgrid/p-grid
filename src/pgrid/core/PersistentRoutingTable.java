package pgrid.core;

import java.io.FileNotFoundException;

/**
 * This is the interface to be implemented by a routing table to gain persistency
 * functionality.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface PersistentRoutingTable {

    /**
     * It opens the given filename and constructs the routing table based on
     * the information stored in the file.
     *
     * @param filename the file to be opened.
     * @throws FileNotFoundException in case of a failed attempt to open the
     *                               file.
     */
    public void load(String filename) throws FileNotFoundException;

    /**
     * Captures the current state of the routing table and writes to the given
     * file.
     *
     * @param filename the file to write.
     * @throws FileNotFoundException in case of a failed attempt to open the
     *                               file.
     */
    public void store(String filename) throws FileNotFoundException;
}
