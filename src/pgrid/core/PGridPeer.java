package pgrid.core;

/**
 * Holds information about a P-Grid peer.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridPeer {
    private final PGridHost host_;
    private final RoutingTable routingTable_;

    public PGridPeer(PGridHost host, RoutingTable routingTable) {
        host_ = host;
        routingTable_ = routingTable;
    }

    public PGridHost getHost() {
        return host_;
    }

    public RoutingTable getRoutingTable() {
        return routingTable_;
    }
}
