package pgrid.core;

/**
 * Every peer has a local PeerClock. When he is about to send a message to a
 * remote peer he must include the current timestamp contained in the PeerClock.
 * This is done to compare and update the references in his routing table.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface PeerClock<T extends Number> {

    public void reset();

    public T getTimestamp();

    public void clockedEvent(T timestamp);
}
