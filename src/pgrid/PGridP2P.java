package pgrid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.communication.PGridCommunicationBuilder;
import pgrid.communication.PGridEndpoint;
import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.RoutingTable;
import pgrid.maintenance.Maintainer;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridP2P {

    private static final Logger logger_ = LoggerFactory.getLogger(PGridP2P.class);

    // Local peer information.
    private final PGridHost localhost_;
    private final RoutingTable localRoutingTable_;
    private boolean isBootstrap_;

    // Communication layer
    private PGridProxy serviceProxy_;
    private PGridEndpoint serviceEndpoint_;
    private Thread endpointThread_;

    // Protocol services.
    private Maintainer maintainer_;
    private Thread maintainerThread_;

    public PGridP2P(InetAddress address, int port) throws UnknownHostException {
        // dummy code
        localhost_ = new PGridHost(address, port);
        localRoutingTable_ = new RoutingTable();
        isBootstrap_ = false;
    }

    public PGridP2P(InetAddress address, int port, boolean isBootstrap) throws UnknownHostException {
        // dummy code
        localhost_ = new PGridHost(address, port);
        localRoutingTable_ = new RoutingTable();
        isBootstrap_ = isBootstrap;
    }

    public void initializeP2PServices() {
        logger_.info("===== P2P layer initialization =================================================");
        setUpCommunication();
        setUpMaintenance();
        logger_.info("P2p layer initialized successfully.");
        logger_.info("================================================================================");
    }

    protected void setUpCommunication() {
        logger_.info("Initializing communication layer...");
        PGridCommunicationBuilder builder = new PGridCommunicationBuilder(
                this,
                localhost_.getAddress().getHostAddress(),
                localhost_.getPort()
        );
        serviceProxy_ = builder.getProxyInstance();
        serviceEndpoint_ = builder.getEndpointInstance();
        serviceEndpoint_.initializeServices();
        endpointThread_ = new Thread(serviceEndpoint_, "Thread-Endpoint");
        endpointThread_.start();
        logger_.info("Communication layer initialized successfully.");
    }

    protected void setUpMaintenance() {
        maintainer_ = Maintainer.createMaintainer(this);
        maintainerThread_ = new Thread(maintainer_, "Thread-Maintainer");
        maintainerThread_.start();
    }

    public PGridHost getLocalHost() {
        return localhost_;
    }

    public RoutingTable getLocalRoutingTable() {
        return localRoutingTable_;
    }

    public PGridProxy getServiceProxy() {
        return serviceProxy_;
    }

    public Maintainer getMaintainer() {
        return maintainer_;
    }

    public void join(String address, int port) {
        logger_.info("Contacting known peer {}:{} to join network.", address, port);
        try {
            maintainer_.join(new PGridHost(address, port));
        } catch (UnknownHostException e) {
            logger_.error("Malformed ip address given {}", e);
        }
    }


    public void shutdown() {
        logger_.info("Peer is shutting down.");
        maintainer_.stopServices();
        logger_.info("Maintenance services shut down.");
        serviceEndpoint_.cancel();
        logger_.info("Communication layer shut down.");
        logger_.info("P-Grid p2p facility shut down.");
    }

    public boolean isBootstrap() {
        return isBootstrap_;
    }

    public void timedRemoteEvent(PGridHost remoteHost) {
        if (remoteHost == null) {
            throw new NullPointerException("Remote host cannot be null.");
        }

        logger_.debug("Timed event RECV from remote peer {}", remoteHost);

        logger_.debug("Updating logical clock");
        localhost_.setTimestamp(remoteHost.getTimestamp());

        logger_.debug("Updating routing table reference if there is one.");
        localRoutingTable_.updateHost(remoteHost);
        logger_.debug("RECV event time: {}", localhost_.getTimestamp());
    }

    public void timedLocalEvent() {
        logger_.debug("Timed event SEND from local peer.");
        localhost_.setTimestamp(localhost_.getTimestamp());
        logger_.debug("SEND event time: {}", localhost_.getTimestamp());
    }
}
