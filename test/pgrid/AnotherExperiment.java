package pgrid;

import org.junit.Test;
import pgrid.core.PGridHost;
import pgrid.core.RoutingTable;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class AnotherExperiment {

    @Test
    public void experiment() throws UnknownHostException {
        Collection<PGridHost> col1 = new ArrayList<PGridHost>();
        PGridHost host1 = new PGridHost("127.0.0.1", 3000);
        host1.setHostPath("1");
        col1.add(host1);
        
        Collection<PGridHost> col2 = new ArrayList<PGridHost>();
        PGridHost host2 = new PGridHost("127.0.0.1", 3000);
        host2.setHostPath("11");
        col2.add(host2);

        Collection<PGridHost> result = RoutingTable.union(col1, col2);
        for (PGridHost host : result) {
            System.out.println(host.getHostPath());
        }

    }
}
