package pgrid;


import org.junit.Test;
import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.UUID;

public class RpcIntegrationTest {

    class MockP2P extends PGridP2P {

        public MockP2P(String address, int port) throws UnknownHostException {
            super(InetAddress.getByName(address), port);
        }

        public void fakeRT() {
            RoutingTable localRT = getLocalRoutingTable();
            String path = "";
            for (int i = 0; i < 6; i++) {
                try {
                    PGridHost host = new PGridHost("127.0.0.1", 9874);
                    path = path + (i % 2);
                    host.setHostPath(path);
                    host.setTimestamp(i);
                    host.setUUID(UUID.randomUUID());
                    localRT.appendReference(host);
                } catch (UnknownHostException e) {
                }
            }
        }
    }

    @Test
    public void testRPCs() throws SocketException, UnknownHostException {
        // Rpc calls to self.
        String address = "localhost";
        int port = 3000;

        MockP2P p2p = null;
        try {
            p2p = new MockP2P(address, port);
        } catch (UnknownHostException e) {

        }

        p2p.fakeRT();
        p2p.initializeP2PServices();
        PGridProxy proxy = p2p.getServiceProxy();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

        RoutingTable rt = proxy.getRemoteRT(address, port); // find self
        for (PGridHost host : rt.getTillLevel(rt.length() - 1)) {
            System.out.println(
                    "[" + host.getTimestamp() + "]" +
                            host.getHostPath().getPath() +
                            " -> " + host.toString() +
                            " {" + host.getUUID() + "}"
            );
        }

        PGridHost host = proxy.getRemoteHost(address, port); // find self
        System.out.println(
                "[" + host.getTimestamp() + "]" +
                        host.getHostPath().getPath() +
                        " -> " + host.toString() +
                        " {" + host.getUUID() + "}"
        );

        boolean result = proxy.isOnline(address, port); // find self
        System.out.println("Remote is online? " + result);

        PGridPeer remote = proxy.exchangeInfo(p2p.getLocalHost(), p2p.getLocalHost(), p2p.getLocalRoutingTable());
        System.out.println("Remote exchange information:");
        System.out.println(remote.getHost());
        RoutingTable remoteRT = remote.getRoutingTable();
        for (PGridHost ref : remoteRT.getTillLevel(remoteRT.length() - 1)) {
            System.out.println("\t" + ref.getHostPath().getPath() + " = " + ref.toString());
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }
        p2p.shutdown();
    }
}
