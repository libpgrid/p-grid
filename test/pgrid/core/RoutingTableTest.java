package pgrid.core;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pgrid.maintenance.exchange.ExchangeAlgorithm;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

public class RoutingTableTest {

    private static RoutingTable routingTable_;

    @Before
    public void before() {
        routingTable_ = new RoutingTable();
    }

    @After
    public void after() {
        routingTable_.clear();
    }


    @Test
    public void testAppendReference() throws UnknownHostException {
        PGridHost host1 = new PGridHost("127.0.0.1", 3001);
        host1.setHostPath("1");
        PGridHost host2 = new PGridHost("127.0.0.1", 3002);
        host2.setHostPath("01");

        routingTable_.appendReference(host1);
        routingTable_.appendReference(host2);

        Assert.assertTrue(routingTable_.getLevel(0) == host1);
        Assert.assertTrue(routingTable_.getLevel(1) == host2);
        Assert.assertTrue(routingTable_.length() == 2);
    }

    @Test
    public void testRemoveLastReference() throws UnknownHostException {
        PGridHost host1 = new PGridHost("127.0.0.1", 3001);
        host1.setHostPath("1");
        PGridHost host2 = new PGridHost("127.0.0.1", 3002);
        host2.setHostPath("01");

        routingTable_.appendReference(host1);
        routingTable_.appendReference(host2);

        Assert.assertTrue(routingTable_.removeLastReference());
        Assert.assertTrue(routingTable_.length() == 1);

        Assert.assertTrue(routingTable_.removeLastReference());
        Assert.assertTrue(routingTable_.length() == 0);

        // routing table empty now, it must return false
        Assert.assertFalse(routingTable_.removeLastReference());
        Assert.assertTrue(routingTable_.length() == 0);
    }

    @Test
    public void testUpdateReference() throws UnknownHostException {
        PGridHost host1 = new PGridHost("127.0.0.1", 3001);
        host1.setHostPath("1");
        PGridHost host2 = new PGridHost("127.0.0.1", 3002);
        host2.setHostPath("01");
        PGridHost hostUpdate = new PGridHost("127.0.0.1", 3003);
        hostUpdate.setHostPath("01");

        routingTable_.appendReference(host1);
        routingTable_.appendReference(host2);

        routingTable_.updateReference(1, hostUpdate);

        Assert.assertTrue(routingTable_.getLevel(1) == hostUpdate);
        Assert.assertTrue(routingTable_.getLevel(1) != host2);
        Assert.assertTrue(routingTable_.getLevel(0) == host1);
        Assert.assertTrue(routingTable_.length() == 2);
    }

    @Test
    public void testGetTillLevel() throws UnknownHostException {
        PGridHost host0 = new PGridHost("127.0.0.1", 3001);
        host0.setHostPath("1");
        PGridHost host1 = new PGridHost("127.0.0.1", 3002);
        host1.setHostPath("01");
        PGridHost host2 = new PGridHost("127.0.0.1", 3003);
        host2.setHostPath("001");
        PGridHost host3 = new PGridHost("127.0.0.1", 3004);
        host3.setHostPath("00001");

        routingTable_.appendReference(host0);
        routingTable_.appendReference(host1);
        routingTable_.appendReference(host2);
        routingTable_.appendReference(host3);

        Vector<PGridHost> list = (Vector<PGridHost>) routingTable_.getTillLevel(2);

        Assert.assertTrue(list.size() == 3);

        Assert.assertTrue(list.get(0) == host0);
        Assert.assertTrue(list.get(1) == host1);
        Assert.assertTrue(list.get(2) == host2);
    }

    @Test
    public void testRandomSelect() throws UnknownHostException {
        PGridHost host0 = new PGridHost("127.0.0.1", 3001);
        host0.setHostPath("1");
        PGridHost host1 = new PGridHost("127.0.0.1", 3002);
        host1.setHostPath("01");
        PGridHost host2 = new PGridHost("127.0.0.1", 3003);
        host2.setHostPath("001");
        PGridHost host3 = new PGridHost("127.0.0.1", 3004);
        host3.setHostPath("00001");

        RoutingTable routingTable = new RoutingTable();
        routingTable.appendReference(host0);
        routingTable.appendReference(host1);
        routingTable.appendReference(host2);
        routingTable.appendReference(host3);

        Collection<PGridHost> tillLevel = routingTable.getTillLevel(routingTable.length() - 1);
        int refMax = routingTable.length();

        ExchangeAlgorithm algorithm = new ExchangeAlgorithm();
        Collection<PGridHost> list = RoutingTable.randomSelect(refMax, tillLevel);

        Assert.assertTrue(!list.isEmpty());
        Assert.assertTrue(list.contains(host0));
        Assert.assertTrue(list.contains(host1));
        Assert.assertTrue(list.contains(host2));
        Assert.assertTrue(list.contains(host3));
    }

    @Test
    public void testUnion() throws UnknownHostException {
        PGridHost host0 = new PGridHost("127.0.0.1", 3001);
        host0.setHostPath("1");
        PGridHost host1 = new PGridHost("127.0.0.1", 3002);
        host1.setHostPath("01");
        PGridHost host2 = new PGridHost("127.0.0.1", 3003);
        host2.setHostPath("001");
        PGridHost host3 = new PGridHost("127.0.0.1", 3004);
        host3.setHostPath("00001");

        RoutingTable routingTable = new RoutingTable();
        routingTable.appendReference(host0);
        routingTable.appendReference(host1);

        RoutingTable remoteRT = new RoutingTable();
        remoteRT.appendReference(host2);
        remoteRT.appendReference(host3);

        int commonRefs = 1;

        Collection<PGridHost> result;
        result = RoutingTable.union(routingTable.getTillLevel(commonRefs), remoteRT.getTillLevel(commonRefs));

        for (int i = 0; i < commonRefs; i++) {
            Assert.assertTrue(result.contains(remoteRT.getLevel(i)));
            Assert.assertTrue(result.contains(routingTable.getLevel(i)));
        }
    }
    
    @Test
    public void testUnion_DifferentLengthPath_SamePrefix() throws UnknownHostException {
        Collection<PGridHost> col1 = new ArrayList<PGridHost>();
        PGridHost host1 = new PGridHost("127.0.0.1", 3000);
        host1.setHostPath("1");
        col1.add(host1);

        Collection<PGridHost> col2 = new ArrayList<PGridHost>();
        PGridHost host2 = new PGridHost("127.0.0.1", 3000);
        host2.setHostPath("11");
        col2.add(host2);

        PGridHost[] result = RoutingTable.union(col1, col2).toArray(new PGridHost[2]);
        Assert.assertTrue(result[0].getHostPath().getPath().compareTo("1") == 0);
        Assert.assertTrue(result[1].getHostPath().getPath().compareTo("11") == 0);
    }

    @Test
    public void testUnion_DifferentLengthPath__DifferentPrefix() throws UnknownHostException {
        Collection<PGridHost> col1 = new ArrayList<PGridHost>();
        PGridHost host1 = new PGridHost("127.0.0.1", 3000);
        host1.setHostPath("0");
        col1.add(host1);

        Collection<PGridHost> col2 = new ArrayList<PGridHost>();
        PGridHost host2 = new PGridHost("127.0.0.1", 3000);
        host2.setHostPath("11");
        col2.add(host2);

        PGridHost[] result = RoutingTable.union(col1, col2).toArray(new PGridHost[2]);
        Assert.assertTrue(result[0].getHostPath().getPath().compareTo("0") == 0);
        Assert.assertTrue(result[1].getHostPath().getPath().compareTo("11") == 0);
    }

    @Test
    public void testUnion_SameLengthPath() throws UnknownHostException {
        Collection<PGridHost> col1 = new ArrayList<PGridHost>();
        PGridHost host1 = new PGridHost("127.0.0.1", 3000);
        host1.setHostPath("01");
        col1.add(host1);

        Collection<PGridHost> col2 = new ArrayList<PGridHost>();
        PGridHost host2 = new PGridHost("127.0.0.1", 3000);
        host2.setHostPath("11");
        col2.add(host2);

        PGridHost[] result = RoutingTable.union(col1, col2).toArray(new PGridHost[2]);
        Assert.assertTrue(result[0].getHostPath().getPath().compareTo("01") == 0);
        Assert.assertTrue(result[1].getHostPath().getPath().compareTo("11") == 0);
    }
}
