package pgrid.core;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class LoadSaveXMLRoutingTableTest {
    private static final String FILE = "test/pgrid/core/routingTable.xml";
    private static List<PGridHost> hostList_;

    @BeforeClass
    public static void beforeClass() throws UnknownHostException {
        PGridHost host0 = new PGridHost("localhost", 3000);
        host0.setHostPath("1");
        host0.setUUID(UUID.fromString("10d82159-3713-4b78-884e-4104720ae2d8"));
        host0.setTimestamp(1);

        PGridHost host1 = new PGridHost("localhost", 3001);
        host1.setHostPath("01");
        host0.setUUID(UUID.fromString("463dad77-627d-4a5d-930b-aab6a4523490"));
        host0.setTimestamp(2);

        PGridHost host2 = new PGridHost("localhost", 3002);
        host2.setHostPath("001");
        host0.setUUID(UUID.fromString("ac49991e-68a0-42ad-9c0e-ab328f193e4e"));
        host0.setTimestamp(3);

        PGridHost host3 = new PGridHost("localhost", 3003);
        host3.setHostPath("0011");
        host0.setUUID(UUID.fromString("5a29fac1-bb15-4cdd-a071-f33f2b847349"));
        host0.setTimestamp(4);

        hostList_ = new ArrayList<PGridHost>(4);
        hostList_.add(host0);
        hostList_.add(host1);
        hostList_.add(host2);
        hostList_.add(host3);
    }

    @AfterClass
    public static void afterClass() {
        File file = new File(FILE);
        file.delete();
    }

    @Test
    public void testStoreRoutingTable() throws UnknownHostException {
        RoutingTable routingTable = new RoutingTable();
        for (PGridHost host : hostList_) {
            routingTable.appendReference(host);
        }

        try {
            routingTable.store(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLoadRoutingTable() {
        RoutingTable rt = new RoutingTable();
        try {
            rt.load(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Assert.assertArrayEquals(hostList_.toArray(), rt.getAllLevels().toArray());

        for (PGridHost host : rt.getAllLevels()) {
            System.out.println(host +
                    " {" + host.getTimestamp() + "}" +
                    " [" + host.getHostPath() + "] " +
                    host.getUUID());
        }
    }


}