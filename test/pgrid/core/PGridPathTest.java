package pgrid.core;

import org.junit.Assert;
import org.junit.Test;

public class PGridPathTest {

    @Test
    public void testCommonPrefix() {
        String result = "00010";
        PGridPath path1 = new PGridPath("000100");
        PGridPath path2 = new PGridPath("0001010");
        Assert.assertTrue(0 == result.compareTo(path1.commonPrefix(path2)));
    }

    @Test
    public void testOthers() {
        String strPath = "000100";
        PGridPath path = new PGridPath(strPath);

        // revert
        Assert.assertTrue('1' == path.revert('0'));
        Assert.assertTrue('0' == path.revert('1'));

        // value
        Assert.assertTrue(strPath.charAt(0) == path.value(0));
        Assert.assertTrue(strPath.charAt(3) == path.value(3));

        // subPath
        int start = 0;
        int end = 3;
        String subResult = strPath.substring(start, end);
        Assert.assertTrue(0 == subResult.compareTo(path.subPath(start, end)));

        // append
        path.append('1');
        String appendResult = strPath + '1';
        Assert.assertTrue(0 == appendResult.compareTo(path.getPath()));

        // constructor with null argument
        PGridPath path0 = new PGridPath(null);
        Assert.assertTrue(0 == "".compareTo(path0.getPath()));
    }
}
