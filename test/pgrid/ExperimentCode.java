package pgrid;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.concurrent.*;

public class ExperimentCode {
    @Ignore
    public void howToGetIP() {
        try {
            NetworkInterface i = NetworkInterface.getByName("eth0");
            System.out.println("Display name: " + i.getDisplayName());
            System.out.println("Name: " + i.getName());
            System.out.println("isPointToPoint: " + i.isPointToPoint());
            System.out.println("isUp: " + i.isUp());
            System.out.println("isVirtual: " + i.isVirtual());
            System.out.println("MTU: " + i.getMTU());
            for (InetAddress inetAddress : Collections.list(i.getInetAddresses())) {
                System.out.println("\tHostname: " + inetAddress.getHostName());
                System.out.println("\tCanonical hostname: " + inetAddress.getCanonicalHostName());
                System.out.println("\tHost address: " + inetAddress.getHostAddress());
                System.out.println("\tisAnyLocalAddress: " + inetAddress.isAnyLocalAddress());
                System.out.println("\tisLinkLocalAddress: " + inetAddress.isLinkLocalAddress());
                System.out.println("\tisLoopbackAddress: " + inetAddress.isLoopbackAddress());
                System.out.println("\tisReachable: " + inetAddress.isReachable(1000));
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class FSM {
        private static final Executor maintenanceExecutor_ = Executors.newSingleThreadExecutor();

        static class PeerMeeting {
            private String name_;
            private boolean online_;
            private MaintenanceState state_ = MaintenanceState.NEW;
            private final Executor executor_;

            public PeerMeeting(Executor executor) {
                executor_ = executor;
            }

            public Executor getExecutor() {
                return executor_;
            }

            public String getName() {
                return name_;
            }

            public void setName(String name) {
                name_ = name;
            }

            public boolean isOnline() {
                return online_;
            }

            public void setValid(boolean valid) {
                online_ = valid;
            }

            public MaintenanceState getState() {
                return state_;
            }

            public void setState(MaintenanceState state) {
                if (state != null && state != state_) {
                    System.out.println("PeerMeeting#" + name_ + ": changing status from " +
                            state_ + " to " + state);
                    state_ = state;
                }
            }

            public void validate() {
                setState(state_.validate(this));
            }

            public void execute() {
                MaintenanceState res = null;
                try {
                    res = state_.exchange(this);
                } catch (UnsupportedOperationException ex) {
                    System.out.println(ex.getMessage());
                    res = state_.repair(this);
                }
                setState(res);
            }

            public void process() {
                validate();
                execute();
            }
        }

        interface MaintenanceOperations {
            public MaintenanceState validate(PeerMeeting meeting);

            public MaintenanceState exchange(PeerMeeting meeting);

            public MaintenanceState repair(PeerMeeting meeting);
        }

        enum MaintenanceState implements MaintenanceOperations {
            NEW(new NewMeeting()),
            EXCHANGE(new ExchangeMeeting()),
            REPAIR(new RepairMeeting()),
            DONE(new DoneMeeting());

            private final MaintenanceOperations operations_;

            MaintenanceState(MaintenanceOperations operations) {
                operations_ = operations;
            }

            @Override
            public MaintenanceState validate(PeerMeeting meeting) {
                return operations_.validate(meeting);
            }

            @Override
            public MaintenanceState exchange(PeerMeeting meeting) {
                return operations_.exchange(meeting);
            }

            @Override
            public MaintenanceState repair(PeerMeeting meeting) {
                return operations_.repair(meeting);
            }
        }

        static class NewMeeting implements MaintenanceOperations {
            @Override
            public MaintenanceState validate(final PeerMeeting meeting) {
                System.out.println("Validating the new peer meeting.");
                Callable<MaintenanceState> validator = new Callable<MaintenanceState>() {
                    @Override
                    public MaintenanceState call() throws Exception {
                        System.out.println("Validating inside a thread!!!!!!");
                        return (meeting.isOnline()) ? MaintenanceState.EXCHANGE : MaintenanceState.REPAIR;
                    }
                };
                CompletionService<MaintenanceState> ecs =
                        new ExecutorCompletionService<MaintenanceState>(meeting.getExecutor());
                ecs.submit(validator);
                MaintenanceState nextState = null;
                try {
                    nextState = ecs.take().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                return nextState;
            }

            @Override
            public MaintenanceState exchange(PeerMeeting meeting) {
                throw new UnsupportedOperationException("Exchange while NEW meeting");
            }

            @Override
            public MaintenanceState repair(PeerMeeting meeting) {
                throw new UnsupportedOperationException("Repair while NEW meeting");
            }
        }

        static class ExchangeMeeting implements MaintenanceOperations {
            @Override
            public MaintenanceState validate(PeerMeeting meeting) {
                throw new UnsupportedOperationException("already validated");
            }

            @Override
            public MaintenanceState exchange(PeerMeeting meeting) {
                System.out.println("I'm doing exchange now. I really do...");
                Callable<MaintenanceState> exchanger = new Callable<MaintenanceState>() {
                    @Override
                    public MaintenanceState call() throws Exception {
                        System.out.println("Exchanging inside a thread!!!!!!");
                        return MaintenanceState.DONE;
                    }
                };
                CompletionService<MaintenanceState> ecs =
                        new ExecutorCompletionService<MaintenanceState>(meeting.getExecutor());
                ecs.submit(exchanger);
                MaintenanceState nextState = null;
                try {
                    nextState = ecs.take().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                return nextState;
            }

            @Override
            public MaintenanceState repair(PeerMeeting meeting) {
                throw new UnsupportedOperationException("validated for an exchange");
            }
        }

        static class RepairMeeting implements MaintenanceOperations {
            @Override
            public MaintenanceState validate(PeerMeeting meeting) {
                throw new UnsupportedOperationException("Already validated");
            }

            @Override
            public MaintenanceState exchange(PeerMeeting meeting) {
                throw new UnsupportedOperationException("validated for a repair");
            }

            @Override
            public MaintenanceState repair(PeerMeeting meeting) {
                System.out.println("I'm repairing broken things now...:S");
                Callable<MaintenanceState> repairer = new Callable<MaintenanceState>() {
                    @Override
                    public MaintenanceState call() throws Exception {
                        System.out.println("Repairing inside a thread!!!!!!");
                        return MaintenanceState.DONE;
                    }
                };
                CompletionService<MaintenanceState> ecs =
                        new ExecutorCompletionService<MaintenanceState>(meeting.getExecutor());
                ecs.submit(repairer);
                MaintenanceState nextState = null;
                try {
                    nextState = ecs.take().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                return nextState;
            }
        }

        static class DoneMeeting implements MaintenanceOperations {
            @Override
            public MaintenanceState validate(PeerMeeting meeting) {
                throw new UnsupportedOperationException("The meeting is over");
            }

            @Override
            public MaintenanceState exchange(PeerMeeting meeting) {
                throw new UnsupportedOperationException("The meeting is over");
            }

            @Override
            public MaintenanceState repair(PeerMeeting meeting) {
                throw new UnsupportedOperationException("The meeting is over");
            }
        }

        public void play() {
            PeerMeeting meeting1 = new PeerMeeting(maintenanceExecutor_);
            meeting1.setName("work");
            meeting1.setValid(true);
            meeting1.process();

            PeerMeeting meeting2 = new PeerMeeting(maintenanceExecutor_);
            meeting2.setName("break");
            meeting2.setValid(false);
            meeting2.process();
        }
    }
}