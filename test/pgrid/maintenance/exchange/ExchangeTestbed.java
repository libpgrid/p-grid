package pgrid.maintenance.exchange;

import pgrid.core.PGridHost;
import pgrid.core.RoutingTable;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * This class is used for simulating purposes in unit tests for the exchange
 * algorithm. Given a CASE {1, 2, 3}, it construct the contexts needed by
 * the Exchange class to run the algorithm. The P-Grid trees that are
 * constructed for each case and are shown bellow.
 * <ul>
 * <li>CASE 1: local == 0 && remote == 0
 * <pre>
 *          0/\1
 *          /  [host1]
 *        0/ \1
 *        /   [host2]
 *     0/   \1
 *   [A|B]   [host3]
 * </pre>
 * </li>
 * <li>CASE 2 & 3: local == 0 && remote > 0</dd>
 * <pre>
 *          0/\1
 *          /  [host1]
 *        0/ \1
 *       [B]  [host2]
 *     0/   \1
 *    [A]    [host3]
 * </pre>
 * </li>
 * <li>CASE 4: local > 0 && remote > 0
 * <pre>
 *          0/\1
 *          /  [host1]
 *        0/ \1
 *        /   [host2]
 *     0/   \1
 *    [A]   [B]
 * </pre>
 * </li>
 * <li>BOOTSTRAP CASE 1: local == 0 && remote == 0
 * <pre>
 *    [A]   [B] (both with path "")
 * </pre>
 * </li>
 * <li>BOOTSTRAP CASE 2 & 3: local > 0 && remote == 0
 * <pre>
 *        [B] (B with path "")
 *      0/
 *    [A]
 * </pre>
 * </li>
 * </ul>
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
class ExchangeTestbed {

    public enum Case {
        CASE_1,
        CASE_2_3,
        CASE_4,
        BOOTSTRAP_1,
        BOOTSTRAP_2
    }

    private static final Map<Integer, UUID> hostUUID_ = new HashMap<Integer, UUID>();

    static {
        for (int i = 1; i <= 5; i++) {
            hostUUID_.put(i, UUID.randomUUID());
        }
    }

    // take the contexts from the public static methods.
    private ExchangeTestbed() {
    }

    public static String expectedLocalPath(Case CASE) {
        String result = null;
        switch (CASE) {
            case CASE_1:
                result = "0000";
                break;
            case CASE_2_3:
                result = "000";
                break;
            case CASE_4:
                result = "000";
                break;
            case BOOTSTRAP_1:
            case BOOTSTRAP_2:
                result = "0";
                break;
        }
        return result;
    }

    public static String expectedRemotePath(Case CASE) {
        String result = null;
        switch (CASE) {
            case CASE_1:
                result = "0001";
                break;
            case CASE_2_3:
                result = "001";
                break;
            case CASE_4:
                result = "001";
                break;
            case BOOTSTRAP_1:
            case BOOTSTRAP_2:
                result = "1";
                break;
        }
        return result;
    }

    public static ExchangeContext createLocalContext(Case CASE) {
        PGridHost localHost = null;
        PGridHost remoteHost = null;
        RoutingTable localRT = null;
        RoutingTable remoteRT = null;

        try {
            localHost = new PGridHost("127.0.0.1", 3000);
            localHost.setUUID(hostUUID_.get(4));
            remoteHost = new PGridHost("127.0.0.1", 3001);
            localHost.setUUID(hostUUID_.get(5));

            localRT = createLocalRT(CASE);
            remoteRT = createRemoteRT(CASE);
        } catch (UnknownHostException ex) {
            // Silence - Not going to be thrown
        }

        setPath(CASE, localHost, remoteHost);
        ExchangeContext context = new ExchangeContext(localHost, localRT, false);
        context.setRemoteInfo(remoteHost, remoteRT);
        return context;
    }

    public static ExchangeContext createRemoteContext(Case CASE) {
        PGridHost localHost = null;
        PGridHost remoteHost = null;
        RoutingTable localRT = null;
        RoutingTable remoteRT = null;

        try {
            localHost = new PGridHost("127.0.0.1", 3000);
            localHost.setUUID(hostUUID_.get(4));
            remoteHost = new PGridHost("127.0.0.1", 3001);
            localHost.setUUID(hostUUID_.get(5));

            localRT = createLocalRT(CASE);
            remoteRT = createRemoteRT(CASE);
        } catch (UnknownHostException ex) {
            // Silence - Not going to be thrown
        }

        setPath(CASE, localHost, remoteHost);
        ExchangeContext context = new ExchangeContext(remoteHost, remoteRT, true);
        context.setRemoteInfo(localHost, localRT);
        return context;
    }

    private static void setPath(Case CASE, PGridHost localHost, PGridHost remoteHost) {
        switch (CASE) {
            case CASE_1:
                localHost.setHostPath("000");
                remoteHost.setHostPath("000");
                break;
            case CASE_2_3:
                localHost.setHostPath("000");
                remoteHost.setHostPath("00");
                break;
            case CASE_4:
                localHost.setHostPath("000");
                remoteHost.setHostPath("001");
                break;
            case BOOTSTRAP_1:
                localHost.setHostPath("");
                remoteHost.setHostPath("");
                break;
            case BOOTSTRAP_2:
                localHost.setHostPath("0");
                remoteHost.setHostPath("");
                break;
        }
    }

    private static RoutingTable createLocalRT(Case CASE) throws UnknownHostException {
        // the following are stored in the LOCAL host
        RoutingTable localRT = new RoutingTable();

        if (CASE.compareTo(Case.BOOTSTRAP_1) == 0) {
            return localRT;
        }


        // CASE == 1: local == 0 && remote == 0
        PGridHost host1 = new PGridHost("127.0.0.1", 1001);
        host1.setHostPath("1");
        host1.setUUID(hostUUID_.get(1));
        localRT.appendReference(host1);

        if (CASE.compareTo(Case.BOOTSTRAP_2) == 0) {
            return localRT;
        }

        PGridHost host2 = new PGridHost("127.0.0.1", 1002);
        host2.setHostPath("01");
        host2.setUUID(hostUUID_.get(2));
        localRT.appendReference(host2);

        PGridHost host3;
        switch (CASE) {
            case CASE_4:
                // CASE == 4: local > 0 && remote > 0
                host3 = new PGridHost("127.0.0.1", 3001); // points to remote host
                break;
            default:
                // CASE == 2 & 3: local == 0 && remote > 0
                host3 = new PGridHost("127.0.0.1", 1003);
                host3.setUUID(hostUUID_.get(3));
                break;
        }

        host3.setHostPath("001");
        localRT.appendReference(host3);

        return localRT;
    }

    private static RoutingTable createRemoteRT(Case CASE) throws UnknownHostException {
        // the following are stored in the REMOTE host
        RoutingTable remoteRT = new RoutingTable();

        if (CASE.compareTo(Case.BOOTSTRAP_1) == 0 || CASE.compareTo(Case.BOOTSTRAP_2) == 0) {
            return remoteRT;
        }

        // CASE == 2 & 3: local == 0 && remote > 0
        PGridHost host1 = new PGridHost("127.0.0.1", 1001);
        host1.setHostPath("1");
        host1.setUUID(hostUUID_.get(1));
        PGridHost host2 = new PGridHost("127.0.0.1", 1002);
        host2.setHostPath("01");
        host2.setUUID(hostUUID_.get(2));

        remoteRT.appendReference(host1);
        remoteRT.appendReference(host2);

        switch (CASE) {
            case CASE_1:
                // CASE == 1: local == 0 && remote == 0
                PGridHost host3 = new PGridHost("127.0.0.1", 1003);
                host3.setHostPath("001");
                remoteRT.appendReference(host3);
                host3.setUUID(hostUUID_.get(3));
                break;
            case CASE_4:
                // CASE == 4: local > 0 && remote > 0
                PGridHost host6 = new PGridHost("127.0.0.1", 3000); // points to local host
                host6.setHostPath("000");
                host6.setUUID(hostUUID_.get(4));
                remoteRT.appendReference(host6);
                break;
        }
        return remoteRT;
    }
}
