package pgrid.maintenance.exchange;


import org.junit.Assert;
import org.junit.Test;


public class ExchangeAlgorithmTest {


    private final ExchangeAlgorithm localExchange_ = new ExchangeAlgorithm();
    private final ExchangeAlgorithm remoteExchange_ = new ExchangeAlgorithm();

    private void executeExchange(ExchangeContext local, ExchangeContext remote) {
        localExchange_.execute(local);
        remoteExchange_.execute(remote);
    }

    private boolean validatePath(ExchangeTestbed.Case CASE, String path, boolean isLocal) {
        String result;
        if (isLocal) {
            result = ExchangeTestbed.expectedLocalPath(CASE);
        } else {
            result = ExchangeTestbed.expectedRemotePath(CASE);
        }
        return path.compareTo(result) == 0;
    }

    @Test
    public void testExecuteCase1() {
        // CASE == 1: local == 0 && remote == 0
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.CASE_1;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        executeExchange(local, remote);

        Assert.assertTrue(validatePath(CASE, local.getLocalHost().getHostPath().getPath(), true));
        Assert.assertTrue(validatePath(CASE, remote.getLocalHost().getHostPath().getPath(), false));

        Assert.assertTrue(local.getLocalRoutingTable().length() == 4);
        Assert.assertTrue(remote.getLocalRoutingTable().length() == 4);
    }

    @Test
    public void testExecuteCase2_3() {
        // CASE == 2: local == 0 && remote > 0
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.CASE_2_3;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        executeExchange(local, remote);

        Assert.assertTrue(validatePath(CASE, local.getLocalHost().getHostPath().getPath(), true));
        Assert.assertTrue(validatePath(CASE, remote.getLocalHost().getHostPath().getPath(), false));

        Assert.assertTrue(local.getLocalRoutingTable().length() == 3);
        Assert.assertTrue(remote.getLocalRoutingTable().length() == 3);
    }

    @Test
    public void testExecuteCase4() {
        // CASE == 4: local > 0 && remote > 0
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.CASE_4;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        executeExchange(local, remote);

        Assert.assertTrue(validatePath(CASE, local.getLocalHost().getHostPath().getPath(), true));
        Assert.assertTrue(validatePath(CASE, remote.getLocalHost().getHostPath().getPath(), false));

        Assert.assertTrue(local.getLocalRoutingTable().length() == 3);
        Assert.assertTrue(remote.getLocalRoutingTable().length() == 3);

        Assert.assertTrue(local.isRecursive());
        Assert.assertTrue(remote.isRecursive());

        Assert.assertTrue(local.getCommonPathLength() == remote.getCommonPathLength());
        Assert.assertTrue(local.getCommonPathLength() == 2);
    }

    @Test
    public void testPeersNotBootstrapped_1() {
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.BOOTSTRAP_1;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        executeExchange(local, remote);

        Assert.assertTrue(validatePath(CASE, local.getLocalHost().getHostPath().getPath(), true));
        Assert.assertTrue(validatePath(CASE, remote.getLocalHost().getHostPath().getPath(), false));

        Assert.assertTrue(local.getLocalRoutingTable().length() == 1);
        Assert.assertTrue(remote.getLocalRoutingTable().length() == 1);

        Assert.assertFalse(local.isRecursive());
        Assert.assertFalse(remote.isRecursive());
    }

    @Test
    public void testPeersNotBootstrapped_2() {
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.BOOTSTRAP_2;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        executeExchange(local, remote);

        Assert.assertTrue(validatePath(CASE, local.getLocalHost().getHostPath().getPath(), true));
        Assert.assertTrue(validatePath(CASE, remote.getLocalHost().getHostPath().getPath(), false));

        Assert.assertTrue(local.getLocalRoutingTable().length() == 1);
        Assert.assertTrue(remote.getLocalRoutingTable().length() == 1);

        Assert.assertFalse(local.isRecursive());
        Assert.assertFalse(remote.isRecursive());
    }


}
