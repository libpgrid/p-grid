package pgrid.maintenance.exchange;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pgrid.PGridP2P;
import pgrid.communication.PGridProxy;
import pgrid.core.PGridHost;
import pgrid.core.PGridPeer;
import pgrid.core.RoutingTable;
import pgrid.maintenance.Maintainer;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class ExchangerTest {

    private static final Logger logger_ = LoggerFactory.getLogger(ExchangerTest.class);

    /**
     * Private class used to pass to the Exchanger the information we want.
     */
    private static class Proxy implements PGridProxy {

        private ExchangeContext context_;

        public Proxy(ExchangeContext context) {
            context_ = context;
        }

        @Override
        public boolean isOnline(String address, int port) {
            return false;
        }

        @Override
        public boolean isOnline(InetAddress address, int port) {
            return false;
        }

        @Override
        public PGridHost getRemoteHost(String address, int port) {
            return null;
        }

        @Override
        public RoutingTable getRemoteRT(String address, int port) {
            return context_.getRemoteRoutingTable();
        }

        @Override
        public String maintenanceState(PGridHost remoteHost) {
            return null;
        }

        @Override
        public PGridPeer exchangeInfo(PGridHost remoteHost, PGridHost localHost, RoutingTable localRT) {
            return new PGridPeer(context_.getRemoteHost(), context_.getRemoteRoutingTable());
        }

        @Override
        public PGridHost getFailedPeer(PGridHost remoteHost) {
            return null;
        }

        @Override
        public void waitingSolution(PGridHost remoteHost, PGridHost localHost) {

        }

        public RoutingTable getRemoteRT(PGridHost remoteHost) {
            return context_.getRemoteRoutingTable();
        }
    }

    private static class MockP2P extends PGridP2P {
        private PGridProxy proxy_;
        private PGridHost localHost_;
        private RoutingTable localRT_;

        public MockP2P(ExchangeContext context, PGridProxy proxy) throws UnknownHostException {
            super(context.getLocalHost().getAddress(), context.getLocalHost().getPort(), true);
            proxy_ = proxy;
            localHost_ = context.getLocalHost();
            localRT_ = context.getLocalRoutingTable();
        }

        @Override
        public PGridHost getLocalHost() {
            return localHost_;
        }

        @Override
        public RoutingTable getLocalRoutingTable() {
            return localRT_;
        }

        @Override
        public PGridProxy getServiceProxy() {
            return proxy_;
        }
    }

    private static class MockMaintainer extends Maintainer {

        protected MockMaintainer(PGridP2P reference) {
            super(reference);
        }
    }

    @Test
    public void testRunCase1() throws UnknownHostException {
        ExchangeTestbed.Case CASE = ExchangeTestbed.Case.CASE_1;

        ExchangeContext local = ExchangeTestbed.createLocalContext(CASE);
        ExchangeContext remote = ExchangeTestbed.createRemoteContext(CASE);

        Proxy localProxy = new Proxy(local);
        MockP2P localP2P = new MockP2P(local, localProxy);
        MockMaintainer localM = new MockMaintainer(localP2P);
        Exchanger localExchanger = new Exchanger(localM, localProxy, local.getLocalHost(), local.getLocalRoutingTable());

        Proxy remoteProxy = new Proxy(remote);

        MockP2P remoteP2P = new MockP2P(remote, remoteProxy);
        Maintainer remoteM = new MockMaintainer(remoteP2P);
        Exchanger remoteExchanger = new Exchanger(remoteM, remoteProxy, remote.getLocalHost(), remote.getLocalRoutingTable());

        Thread localThread = new Thread(localExchanger, "Local Exchanger");
        Thread remoteThread = new Thread(remoteExchanger, "Remote Exchanger");

        localThread.start();
        localExchanger.inviteHost(local.getRemoteHost());

        try {
            Thread.sleep(15);
        } catch (InterruptedException e) {
            logger_.error("{}", e);
        }
        localExchanger.cancel();

        // will exit AFTER the exchange task is completed.
        remoteThread.start();
        remoteExchanger.addRequest(remote.getRemoteHost(), remote.getRemoteRoutingTable());
        remoteExchanger.cancel();

        try {
            localThread.join(50);
            remoteThread.join(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
